package com.xianzaishi.wms.common.utils;

import com.xianzaishi.wms.common.exception.BizException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils extends org.apache.commons.lang.StringUtils {
	private static Pattern NAME_PATTERN = null;

	public enum Charset {
		US_ASCII("US_ASCII"), ISO_8859_1("ISO_8859_1"), UTF_8("UTF_8"), UTF_16BE(
				"UTF_16BE"), UTF_16LE("UTF_16LE"), UTF_16("UTF_16"), GBK("GBK");

		private String value = null;

		private Charset(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

	static {
		NAME_PATTERN = Pattern.compile("[0-9a-zA-Z\u4e00-\u9fa5]+");
	}

	private String getCharsetForString(String string) {
		byte[] bytes = string.getBytes();
		return null;
	}

	/**
	 * 将多个字符串连接起来,中间用separator指定的字符隔开.
	 * <ul>
	 * <li>如果fields的相邻两个元素开头或结尾本身就是separator,则不再添加separator.
	 * <li>如果fields的相邻两个元素的前一个以separator结束且后一个以separator开始, 则只保留一个separator.
	 * <li>如果第1个元素以separator开头,则会截掉开头的separator.
	 * <li>如果最后一个元素以separator结尾,则会截掉结尾的separator.
	 * </ul>
	 * exmaples:
	 * 
	 * <pre>
	 * join(&quot;/&quot;, &quot;/a&quot;, &quot;b/&quot;, &quot;/c/&quot;) = a / b / c
	 * </pre>
	 * 
	 * @param separator
	 * @param fields
	 * @return
	 */
	public static String join(String separator, String... fields) {
		if (fields == null)
			return null;
		StringBuffer s = new StringBuffer();
		for (int i = 0; i < fields.length; i++) {
			String cur = fields[i];
			if (cur.startsWith(separator))
				cur = cur.substring(separator.length());
			if (cur.endsWith(separator))
				cur = cur.substring(0, cur.length() - separator.length());
			s.append(cur);
			if (i < fields.length - 1)
				s.append(separator);
		}
		return s.toString();
	}

	/**
	 * 字符串校驗,可包含數字、字母、中文
	 */
	public static Boolean checkName(String name) {
		Matcher m = NAME_PATTERN.matcher(name);
		return m.matches();
	}

	/**
	 * 将字符编码转换成US-ASCII码
	 */
	public String toASCII(String str) throws UnsupportedEncodingException {
		return this.changeCharset(str, Charset.US_ASCII.getValue());
	}

	/**
	 * 将字符编码转换成ISO-8859-1码
	 */
	public String toISO_8859_1(String str) throws UnsupportedEncodingException {
		return this.changeCharset(str, Charset.ISO_8859_1.getValue());
	}

	/**
	 * 将字符编码转换成UTF-8码
	 */
	public String toUTF_8(String str) throws UnsupportedEncodingException {
		return this.changeCharset(str, Charset.UTF_8.getValue());
	}

	/**
	 * 将字符编码转换成UTF-16BE码
	 */
	public String toUTF_16BE(String str) throws UnsupportedEncodingException {
		return this.changeCharset(str, Charset.UTF_16BE.getValue());
	}

	/**
	 * 将字符编码转换成UTF-16LE码
	 */
	public String toUTF_16LE(String str) throws UnsupportedEncodingException {
		return this.changeCharset(str, Charset.UTF_16LE.getValue());
	}

	/**
	 * 将字符编码转换成UTF-16码
	 */
	public String toUTF_16(String str) throws UnsupportedEncodingException {
		return this.changeCharset(str, Charset.UTF_16.getValue());
	}

	/**
	 * 将字符编码转换成GBK码
	 */
	public String toGBK(String str) throws UnsupportedEncodingException {
		return this.changeCharset(str, Charset.GBK.getValue());
	}

	/**
	 * 字符串编码转换的实现方法
	 * 
	 * @param str
	 *            待转换编码的字符串
	 * @param newCharset
	 *            目标编码
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String changeCharset(String str, String newCharset)
			throws UnsupportedEncodingException {
		if (str != null) {
			// 用默认字符编码解码字符串。
			byte[] bs = str.getBytes();
			// 用新的字符编码生成字符串
			return new String(bs, newCharset);
		}
		return null;
	}

	/**
	 * 字符串编码转换的实现方法
	 * 
	 * @param str
	 *            待转换编码的字符串
	 * @param oldCharset
	 *            原编码
	 * @param newCharset
	 *            目标编码
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String changeCharset(String str, String oldCharset, String newCharset)
			throws UnsupportedEncodingException {
		if (str != null) {
			// 用旧的字符编码解码字符串。解码可能会出现异常。
			byte[] bs = str.getBytes(oldCharset);
			// 用新的字符编码生成字符串
			return new String(bs, newCharset);
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println(StringUtils.checkName("."));
	}

	// public static void main(String[] s){
	// System.out.println(join("/", "/a", "b/", "/c/"));
	// }

	/**
	 * 判断字符串是否为空
	 * @param obj
	 */
	public static void isEmptyString(String obj,String showMsg) {
		if (null == obj || "".equals(obj) || "null".equals(obj)) {
			throw new BizException(showMsg);
		}
	}
}
