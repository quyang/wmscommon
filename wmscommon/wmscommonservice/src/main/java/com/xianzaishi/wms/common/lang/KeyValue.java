package com.xianzaishi.wms.common.lang;

import java.io.Serializable;

/**
 * 表示一个键值对数据结构
 * @author lirui
 *
 * @param <K>
 * @param <V>
 */
public class KeyValue<K,V> implements Serializable{
	private static final long serialVersionUID = 1L;
	private K k;
	private V v;
	
	public KeyValue(){
		
	}
	
	public KeyValue(K k, V v) {
		super();
		this.k = k;
		this.v = v;
	}

	public K getKey() {
		return k;
	}

	public V getValue() {
		return v;
	}
	
	public void setKey(K k){
		this.k = k;
	}
	
	public void setValue(V v){
		this.v = v;
	}

}
