package com.xianzaishi.wms.common.utils.pixel.impl;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.PixelGrabber;

import javax.swing.ImageIcon;

import org.apache.commons.lang.math.RandomUtils;

import com.xianzaishi.wms.common.utils.pixel.ImageRender;

public class SineImageRender implements ImageRender {
	int w, h; // 图像大小： w为宽，h为高
	int[] temp;// 图像缓存数组
	int[] deal;// 处理后图像数组
	boolean switchYaxis = false;
	
public SineImageRender(){}
	public boolean render(Image srcImg) {
		ImageIcon image = new ImageIcon(srcImg);
		w = image.getIconWidth();// 获取图像信息
		h = image.getIconHeight();
		deal = new int[w * h];
		temp = new int[w * h];
		handlepixels(srcImg, 0, 0, w, h);// 抓取图像像素
		transform();// 插值
		temp = null;
		// 应用变换
		if (srcImg instanceof BufferedImage) {
			((BufferedImage) srcImg).setRGB(0, 0, w, h, deal/* 数组 */, 0, w);
		} else {
			BufferedImage bufImage = new BufferedImage(w, h,
					BufferedImage.TYPE_INT_RGB);
			bufImage.setRGB(0, 0, w, h, deal/* 数组 */, 0, w);
			Graphics g2 = srcImg.getGraphics();
			g2.drawImage(bufImage, 0, 0, null);
			g2.dispose();

		}
		return true;
	}

	private void handlepixels(Image img, int x, int y, int w, int h) // 抓取图像像素
	{
		PixelGrabber pg = new PixelGrabber(img, x, y, w, h, temp, 0, w);
		try {
			pg.grabPixels();
		} catch (InterruptedException e) {
			System.err.println("interrupted waiting for pixels!");
			return;
		}
		if ((pg.getStatus() & ImageObserver.ABORT) != 0) {
			System.err.println("image fetch aborted or errored");
			return;
		}
	}

	private void transform() // 变换
	{

		for (int i = 0; i < deal.length; i++) {
			deal[i] = temp[0];
		}
		
		double[] fac = calTranFactor();
		
		int i, j;
		for (j = 0; j < h; j++) { // 循环逐个扫描像素点
			for (i = 0; i < w; i++) {
				double s = fac[i];
				int y = j ;
				
				if(switchYaxis) y -= (int)(j * s);
				else y *= s;
				if (y < 0)
					y = 0;
				if (y >= h)
					y = h-1 ;
				//把该点移到别处去
				deal[y * w + i] = temp[j*w+i];//average(getAdjacentPoints(i, j)); 
			}
		}
	}

	//计算每一个横坐标对应的纵坐标变换因子
	private double[] calTranFactor() {
		double[] fac = new double[w];
		switchYaxis = RandomUtils.nextBoolean();
		for (int i = 0; i < w; i++){
			if(!switchYaxis){
				double s = Math.sin( i  / (w*2/Math.PI ) + Math.PI/8) ;
				s*= Math.sin( i  / (w*2/Math.PI)  + Math.PI/8) ;
				fac[i] = s;
			}
			else{
				double s = Math.sin( i  / (w*2/Math.PI ) - Math.PI/8) ;
				s*= Math.sin( i  / (w*2/Math.PI)  - Math.PI/8) ;
				fac[i] = s;
			}
		}
		return fac;
	}

}
