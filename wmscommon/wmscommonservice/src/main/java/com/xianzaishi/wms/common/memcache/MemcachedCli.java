package com.xianzaishi.wms.common.memcache;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.whalin.MemCached.MemCachedClient;

public class MemcachedCli {

	private static MemcachedCli unique = new MemcachedCli();

	private MemcachedCli() {
		init();
	}

	public static MemcachedCli getInstance() {
		return unique;
	}

	private MemCachedClient client = new MemCachedClient();

	private void init() {
		client.setPrimitiveAsString(true);
		client.setCompressEnable(true);
		client.setCompressThreshold(4 * 1024);
	}

	public boolean set(String key, Object value) {
		return client.set(key, value);
	}

	public boolean set(String key, Object value, Date expired) {
		return client.set(key, value, expired);
	}

	public Object get(String key) {
		return client.get(key);
	}

	public void printStat() {
		Map stats = client.stats();
		Set keys = stats.keySet();
		Iterator keyIter = keys.iterator();
		while (keyIter.hasNext()) {
			String key = (String) keyIter.next();
			Object value = stats.get(key);
			System.out.println(key + "=" + value);
		}
	}

	public static void main(String[] args) {
		try {
			MemcachedServer server = new MemcachedServer("localhost", 11211, 1);
			List<MemcachedServer> servers = new ArrayList<MemcachedServer>();
			servers.add(server);
			MemcachedPool pool = MemcachedPool.getInstance();
			pool.initPool(servers);
			MemcachedCli client = MemcachedCli.getInstance();
			String value = (String) client.get("test1");
			System.out.println("value=" + value);
			client.set("test1", "value1");
			value = (String) client.get("test1");
			System.out.println("value=" + value);
			client.printStat();
		} catch (MemcachedException e) {
			e.printStackTrace();
		}
	}

}