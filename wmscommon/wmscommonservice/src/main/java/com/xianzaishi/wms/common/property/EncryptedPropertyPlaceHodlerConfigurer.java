package com.xianzaishi.wms.common.property;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;

import com.xianzaishi.wms.common.utils.BASE64;

public class EncryptedPropertyPlaceHodlerConfigurer extends
		PropertyPlaceholderConfigurer {
	private static final Log log = LogFactory
			.getLog(EncryptedPropertyPlaceHodlerConfigurer.class);

	private boolean needDecode(String placeholder) {
		return placeholder != null
				&& placeholder.toUpperCase().endsWith("PASSWORD");
	}

	@Override
	protected String resolvePlaceholder(String placeholder, Properties props) {
		String val = super.resolvePlaceholder(placeholder, props);
		if (needDecode(placeholder)) {
			val = decode(val);
		}
		log.debug(placeholder + "=" + val);
		return val;
	}

	@Override
	protected String resolveSystemProperty(String key) {
		String val = super.resolveSystemProperty(key);
		if (needDecode(key)) {
			return decode(val);
		} else
			return val;
	}

	private String decode(String val) {
		return BASE64.decode(val);
	}

	@Override
	public void setLocations(Resource[] locations) {
		super.setLocations(locations);
	}

	@Override
	protected void loadProperties(Properties props) throws IOException {
		try {
			super.loadProperties(props);
		} catch (Exception e) {
			log.error(e.getMessage());
			System.err.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		System.out.println(BASE64.encode("xzs16888"));
	}
}
