package com.xianzaishi.wms.common.utils.worker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xianzaishi.wms.common.vo.SimpleResultVO;

public abstract class Worker implements Runnable {
	private static Log log = LogFactory.getLog(Worker.class);
	protected Object parentLock = new Object();
	protected Long excute_Time = null;
	protected volatile SimpleResultVO result = null;
	protected volatile boolean success = false;

	public void run() {
		try {
			result = execute();
			success = true;
		} catch (Exception e) {
			log.error("get data error : ", e);
		}
		synchronized (parentLock) {
			parentLock.notify();
		}
	}

	public abstract SimpleResultVO execute() throws Exception;

	public SimpleResultVO getResult() {
		return result;
	}

	public void setResult(SimpleResultVO result) {
		this.result = result;
	}

	public Object getParentLock() {
		return parentLock;
	}

	public void setParentLock(Object parentLock) {
		this.parentLock = parentLock;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public abstract long getExecuteTime();

	public Long getExcute_Time() {
		return excute_Time;
	}

	public void setExcute_Time(Long excute_Time) {
		this.excute_Time = excute_Time;
	}

}
