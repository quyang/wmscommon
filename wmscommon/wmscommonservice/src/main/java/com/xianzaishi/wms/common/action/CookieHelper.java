package com.xianzaishi.wms.common.action;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * cookie输出
 * @author lirui
 *
 */
public class CookieHelper {
	private static final Log log = LogFactory.getLog(CookieHelper.class);
	/**
	 * 向客户端输出一个会话周期内有效的cookie
	 * @param response
	 * @param name
	 * @param value
	 */
	public static void setSessionCookie(HttpServletResponse response, 
			String name, 
			String value){
		setCookie(response, name, value, null, null);
	}
	
	/**
	 * 在Root path(/)下添加一个cookie
	 * @param response
	 * @param name
	 * @param value
	 * @param expiry 有效时间.如果为null, 则为会话有效.
	 */
	public static void setCookieInRoot(HttpServletResponse response, 
			String name, 
			String value,
			Integer expiry
			){
		setCookie(response, name, value, "/", expiry);
	}
	
	/**
	 * 向客户端输出一个指定生命周期的cookie
	 * @param response
	 * @param name
	 * @param value
	 * @param expiry 有效时间.如果为null, 则为会话有效.
	 */
	public static void setCookie(HttpServletResponse response, 
			String name, 
			String value,
			String path,
			Integer expiry){
		Cookie cookie = new Cookie(name, value);
		if(path == null || path.trim().length() == 0){
			cookie.setPath("/");
		}
		else
			cookie.setPath(path);
		cookie.setDomain(".loveyfruit.com");
		if(expiry != null)
			cookie.setMaxAge(expiry);
		response.addCookie(cookie);
		if(log.isDebugEnabled())
			log.debug("Set cookie:"+name+"="+value);
	}
	
	public static String getCookie(HttpServletRequest request,String name){
		String cookieValue=null;
		Cookie[] cookies = request.getCookies();
		if(cookies!=null){
			for(Cookie c :cookies ){
	            if(name.equalsIgnoreCase(c.getName())){
	            	cookieValue = c.getValue();
	            	break;
	            }
	        }
		}
		return cookieValue;
	}
}
