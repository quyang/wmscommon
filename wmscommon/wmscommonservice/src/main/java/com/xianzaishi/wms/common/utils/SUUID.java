package com.xianzaishi.wms.common.utils;

import java.util.UUID;

/**
 * 压缩版UUID. 基于java.util.UUID计算而出.字符串由0-9A-Z组成.长度为23~25位.
 * @author lirui
 *
 */
public class SUUID {
	public static char[] chars = new char[] {'0', '1', '2', '3', '4', '5',
		'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
		'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
		'W', 'X', 'Y', 'Z' };
	
	public static char[] charsAll = new char[] {'0', '1', '2', '3', '4', '5',
		'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
		'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
		'W', 'X', 'Y', 'Z' ,'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
		'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
		'w', 'x', 'y', 'z'};

	/**
	 * 返回随机UUID字符串. 只使用0-9和大写字母长度23~25位.
	 * @return
	 */
	public static String random() {
		UUID uuid = UUID.randomUUID();
		return encode(uuid.getMostSignificantBits(), uuid.getLeastSignificantBits(), chars);
	}

	/**
	 * 返回随机UUID字符串. 使用0-9和大小写字母长度23~25位.
	 * @return
	 */
	public static String randomCaseSensitive() {
		UUID uuid = UUID.randomUUID();
		return encode(uuid.getMostSignificantBits(), uuid.getLeastSignificantBits(), charsAll);
	}
	/**
	 * @param mostSigBits
	 * @param leastSigBits
	 * @return
	 */
	private static String encode(long mostSigBits, long leastSigBits, char[] chars){
		return convert(mostSigBits, chars)+convert(leastSigBits, chars);
	}
	
	/**
	 * 按36进制编码
	 * @param val
	 * @param digits
	 * @return
	 */
    private static String convert(long val, char[] chars) {
    	val &= 0x7FFFFFFFFFFFFFFFL; //make sure positive
    	int radix = chars.length;
    	char[] buf = new char[64];
    	int pos = 64;
    	do{
    		int mod = (int)(val % radix);
    		buf[--pos] = chars[ mod ];
    		val = (val / radix);
    	}while(val > 0 && pos > -1);
    	return new String(buf, pos, (64-pos));
    }
    public static void main(String[] args){
    	for (int i = 0; i < 100; i++) {
    		String s = randomCaseSensitive();
    		System.out.println(s.length()+":"+ s);
    	}
    }
}
