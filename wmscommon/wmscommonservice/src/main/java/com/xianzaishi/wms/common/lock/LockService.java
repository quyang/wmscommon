package com.xianzaishi.wms.common.lock;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.memcache.ICacheOperator;

public class LockService implements ILockService {
	@Autowired
	private ICacheOperator cacheOperator = null;

	public Boolean lock(String key, String sign) {
		return cacheOperator.add(key, sign, 2);
	}

	public Boolean release(String key, String sign) {
		Boolean target = false;
		String flag = (String) cacheOperator.get(key);
		if (sign != null && sign.equals(flag)) {
			target = cacheOperator.delete(key);
		}
		return target;
	}

	public ICacheOperator getCacheOperator() {
		return cacheOperator;
	}

	public void setCacheOperator(ICacheOperator cacheOperator) {
		this.cacheOperator = cacheOperator;
	}

}
