package com.xianzaishi.wms.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TimeUtils {
	private static final Log log = LogFactory.getLog(TimeUtils.class);
	private final static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public static Date getDate(String sDate) {
		Date date = null;
		try {
			if (sDate != null && !StringUtils.isEmpty(sDate)) {
				date = simpleDateFormat.parse(sDate);
			}
		} catch (Exception e) {
			log.error("时间格式错误:", e);
		}
		return date;
	}

	public static String getDateString(Date date) {
		String sDate = null;
		try {
			if (date != null) {
				sDate = simpleDateFormat.format(date);
			}
		} catch (Exception e) {
			log.error("时间格式错误:", e);
		}
		return sDate;
	}
}
