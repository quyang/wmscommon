package com.xianzaishi.wms.common.utils;

import com.xianzaishi.wms.common.exception.BizException;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by quyang on 2017/5/6.
 */
public class DemicalUtils {

  public static final int ROUND_DOWN = BigDecimal.ROUND_DOWN;
  public static final int ROUND_UP = BigDecimal.ROUND_UP;
  public static final int ROUND_HALF_UP = BigDecimal.ROUND_HALF_UP;
  public static final int ROUND_HALF_DOWN = BigDecimal.ROUND_HALF_DOWN;


  /**
   * a 乘以b
   */
  public static BigDecimal multiply(String chengshu, String beiChengShu) {
    StringUtils.isEmptyString(chengshu,"乘数为空");
    StringUtils.isEmptyString(beiChengShu,"被乘数为空");
    BigDecimal bigDecimal = new BigDecimal(chengshu);
    BigDecimal bigDecimal2 = new BigDecimal(beiChengShu);
    BigDecimal multiply = bigDecimal.multiply(bigDecimal2);
    return multiply;
  }

  /**
   * a 除以b
   */
  public static BigDecimal divide(String chushu, String beiChuShu, Integer scale, int strategy) {
    StringUtils.isEmptyString(chushu,"除数为空");
    StringUtils.isEmptyString(beiChuShu,"被除数为空");
    hasStrategy(strategy);

    BigDecimal bigDecimal = new BigDecimal(chushu);
    BigDecimal bigDecimal2 = new BigDecimal(beiChuShu);

    BigDecimal multiply = null;
    switch (strategy) {
      case ROUND_DOWN:
        multiply = bigDecimal.divide(bigDecimal2, scale, BigDecimal.ROUND_DOWN);//只取小数点后scale位，不管下一位
        break;
      case ROUND_UP:
        multiply = bigDecimal
            .divide(bigDecimal2, scale, BigDecimal.ROUND_UP);//只要小数点scale位后面有数字就向上进一位，0除过
        break;
      case ROUND_HALF_DOWN:
        multiply = bigDecimal.divide(bigDecimal2, scale,
            BigDecimal.ROUND_HALF_DOWN);//scale位后的下一位  大于等于  0.5就会向上进一位，否则只取小数点后scale位
        break;
      case ROUND_HALF_UP:
        multiply = bigDecimal
            .divide(bigDecimal2, scale,
                BigDecimal.ROUND_HALF_UP);//取小数点后scale位，除非下一位   大于   0.5才会向上进一位
        break;
    }

    if (null == multiply) {
      throw new BizException("除法运算失败");
    }

    return multiply;
  }

  /**
   * a 加b
   */
  public static BigDecimal add(String jiashu, String beiJiaShu) {
    StringUtils.isEmptyString(jiashu,"加数为空");
    StringUtils.isEmptyString(beiJiaShu,"被加数为空");
    BigDecimal bigDecimal = new BigDecimal(jiashu);
    BigDecimal bigDecimal2 = new BigDecimal(beiJiaShu);
    BigDecimal multiply = bigDecimal.add(bigDecimal2);
    return multiply;
  }

  /**
   * a 减b
   */
  public static BigDecimal subtract(String jianshu, String beiJianShu) {
    StringUtils.isEmptyString(jianshu,"减数为空");
    StringUtils.isEmptyString(beiJianShu,"被减数为空");
    BigDecimal bigDecimal = new BigDecimal(jianshu);
    BigDecimal bigDecimal2 = new BigDecimal(beiJianShu);
    BigDecimal multiply = bigDecimal.subtract(bigDecimal2);
    return multiply;
  }

  /**
   * 保留2位小数，第三位四舍五入
   */
  public static String setScaleWith2Point(String target) {
    StringUtils.isEmptyString(target,"数字为空");
    DecimalFormat format = new DecimalFormat("#.00");
    return format.format(target);
  }


  private static void hasStrategy(int strategy) {
    if (ROUND_DOWN == strategy || ROUND_UP == strategy || ROUND_HALF_DOWN == strategy
        || ROUND_HALF_UP == strategy) {
    } else {
      throw new BizException("除法运算中不含策略：" + strategy);
    }
  }


  /**
   * 比较a是否大于b
   */
  public static void greater(String a, String b) {
    StringUtils.isEmptyString(a,"数值" + a + "为空");
    StringUtils.isEmptyString(b,"数值" + b + "为空");
    if (!(new BigDecimal(a).compareTo(new BigDecimal(b)) == 1)) {//
      throw new BizException("数字" + a + "不大于" + b);
    }
  }

  /**
   * 比较a是否大于等于b
   */
  public static void greaterAndEqual(String a, String b) {
    StringUtils.isEmptyString(a, "数值" + a + "为空");
    StringUtils.isEmptyString(b, "数值" + b + "为空");
    if (!(new BigDecimal(a).compareTo(new BigDecimal(b)) >= 1)) {//
      throw new BizException("数字" + a + "不大于等于" + b);
    }
  }

  /**
   * 比较a是否小于b
   */
  public static void less(String a, String b) {
    StringUtils.isEmptyString(a,"数值" + a + "为空");
    StringUtils.isEmptyString(b,"数值" + b + "为空");
    if (!(new BigDecimal(a).compareTo(new BigDecimal(b)) == -1)) {//
      throw new BizException("数字" + a + "不小于" + b);
    }
  }

  /**
   * 比较a是否小于等于b
   */
  public static void lessAndEqual(String a, String b) {
    StringUtils.isEmptyString(a,"数值" + a + "为空");
    StringUtils.isEmptyString(b,"数值" + b + "为空");
    if (!(new BigDecimal(a).compareTo(new BigDecimal(b)) <= 0)) {//
      throw new BizException("数字" + a + "不小于等于" + b);
    }
  }
}
