package com.xianzaishi.wms.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class NumberUtils {
	private static DecimalFormat df1 = new DecimalFormat("#0.00");

	public NumberUtils() {
	}

	public static String numberFormat(Number number) {
		if (number != null)
			return df1.format(number);
		return null;
	}

	public static String numberFormat(Long number) {
		if (number != null)
			return df1.format(number);
		return null;
	}

	public static String numberFormat(Number number, int op, int point) {
		if (number != null) {
			BigDecimal temp = new BigDecimal(number.toString());
			if (op == 0) {
				return df1.format(temp.movePointLeft(point));
			} else {
				return df1.format(temp.movePointRight(point));
			}
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println(NumberUtils.numberFormat(1111111l, 0, 1));
	}
}
