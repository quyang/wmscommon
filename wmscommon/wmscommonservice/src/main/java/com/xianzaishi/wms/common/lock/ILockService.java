package com.xianzaishi.wms.common.lock;

public interface ILockService {
	public Boolean lock(String key, String sign);

	public Boolean release(String key, String sign);
}
