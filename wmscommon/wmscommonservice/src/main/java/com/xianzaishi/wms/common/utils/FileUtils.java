package com.xianzaishi.wms.common.utils;

import java.io.File;

public class FileUtils {
	
	/**
	 * 根据文件路径在本地磁盘创建文件.如果文件目录不存在,则先创建目录.
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public static File createFile(String path) throws Exception {
		File file = new File(path);
		if (!file.exists()) {
			String tmp_path = path.substring(0, path.lastIndexOf("/"));
			File tmp_file = new File(tmp_path);
			if (!tmp_file.exists() || !tmp_file.isDirectory()) {
				tmp_file.mkdirs();
			}
			file.createNewFile();
		}
		return file;
	}
}
