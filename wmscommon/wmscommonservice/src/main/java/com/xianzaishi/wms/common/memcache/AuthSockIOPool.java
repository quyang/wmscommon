package com.xianzaishi.wms.common.memcache;

import com.schooner.MemCached.AuthInfo;
import com.schooner.MemCached.SchoonerSockIOPool;
import com.whalin.MemCached.SockIOPool;

public class AuthSockIOPool extends SockIOPool {

	private AuthInfo authInfo;

	public AuthInfo getAuthInfo() {
		return authInfo;
	}

	public void setAuthInfo(AuthInfo authInfo) {
		this.authInfo = authInfo;
	}

}
