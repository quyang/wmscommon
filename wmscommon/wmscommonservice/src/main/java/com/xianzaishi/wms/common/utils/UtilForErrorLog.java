package com.xianzaishi.wms.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UtilForErrorLog {
	private static Log log = LogFactory.getLog(UtilForErrorLog.class);

	private static IDealWithLogInfo errorInfoDealBean = null;

	private static UtilForErrorLog util = null;

	private static final String errorPath = "d://work/message/receive/";

	private static final String errorFile = "d://work/message/receive/message.txt";

	private static final byte[] readLock = new byte[0];

	private static File file = null;

	private static File backUpFile = null;

	private static final String backUpPath = "d://work/message/backup/";

	private static final String errorBackPath = "d://work/message/error/";

	private static long length = 1 * 1024 * 1024;

	private static FileWriter fileWriter = null;

	private static long latestWriteTime = 0;

	private static long latestReadTime = 0;

	private static long writeDelay = 5000;

	private static long readDelay = 5000;

	public static final String sep = " ";

	public static final String newSep = "|";

	private static final byte[] writeLock = new byte[0];

	private static boolean run = true;

	private static String LINE_SEPARATOR = System.getProperty("line.separator");

	private UtilForErrorLog() {

	}

	public static void writeErrorDate(String errorInfo) throws Exception {
		synchronized (writeLock) {
			if (file == null) {
				file = openFile(errorFile);
				fileWriter = new FileWriter(file, true);
			}
			fileWriter.write(errorInfo);
			fileWriter.write("\r\n");
			fileWriter.flush();
		}
	}

	public static File openFile(String filename) throws Exception {
		File flag = null;
		try {
			flag = new File(filename);
			if (!flag.exists()) {
				flag.createNewFile();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return flag;
	}

	public static File openDir(String dirName) {
		File flag = new File(dirName);
		if (!flag.exists() || !flag.isDirectory()) {
			flag.mkdir();
		}
		return flag;
	}

	private static class ThreadForRead extends Thread {
		public ThreadForRead() {
			super();
			this.setDaemon(true);
		}

		public void run() {
			while (run) {
				try {
					synchronized (readLock) {
						long wait = latestReadTime + readDelay
								- System.currentTimeMillis();
						boolean flag = false;
						if (wait <= 0) {
							try {
								if (errorInfoDealBean != null) {
									flag = true;
									File f_temp = null;
									backUpFile = openDir(backUpPath);
									while (backUpFile.listFiles() != null
											&& backUpFile.listFiles().length > 0) {
										f_temp = backUpFile.listFiles()[0];
										dealWithErrorInfo(f_temp);
										f_temp.delete();
										backUpFile = openDir(backUpPath);
									}
								}
							} catch (Exception e) {
								log.error("read log error :", e);
							}
							latestReadTime = System.currentTimeMillis();
						}
						try {
							readLock.wait(flag ? readDelay : (wait > 0 ? wait
									: readDelay));
						} catch (InterruptedException e) {
							log.error("read log error :", e);
							interrupt();
						}
					}
				} catch (Exception e) {
					log.error("read log error :", e);
				}
			}
		}
	}

	private static class ThreadForWrite extends Thread {
		public ThreadForWrite() {
			super();
			this.setDaemon(true);
		}

		public void run() {
			while (run) {
				try {
					synchronized (writeLock) {
						long wait = latestWriteTime + writeDelay
								- System.currentTimeMillis();
						boolean flag = false;
						if ((wait <= 0 && file != null && file.length() > 0)
								|| (file != null && file.length() > length)) {
							try {
								flag = true;
								if (fileWriter != null) {
									try {
										fileWriter.close();
										fileWriter = null;
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
								if (file.length() > 0) {
									openDir(backUpPath);
									File backFile = new File(backUpPath
											+ latestWriteTime + ".txt");
									if (!backFile.exists()) {
										file.renameTo(backFile);
									}
								}
								if (file != null) {
									file = null;
								}
							} catch (Exception e) {
								log.error("read log error :", e);
							}
							latestWriteTime = System.currentTimeMillis();
						}
						try {
							writeLock.wait(flag ? writeDelay : (wait > 0 ? wait
									: writeDelay));
						} catch (InterruptedException e) {
							e.printStackTrace();
							interrupt();
						}
					}
				} catch (Exception e) {
					log.error("write log error :", e);
				}
			}
		}
	}

	private static FileWriter getErrorBackFileWriter() throws Exception {
		FileWriter error_back_file_writer = null;
		openDir(errorBackPath);
		File temp = openFile(errorBackPath + System.currentTimeMillis()
				+ ".txt");
		error_back_file_writer = new FileWriter(temp, true);
		return error_back_file_writer;
	}

	private static void dealWithErrorInfo(File file) {
		FileInputStream fis = null;
		InputStreamReader isr = null;
		Scanner scanner = null;
		File ftemp = null;
		FileWriter error_back_file_writer = null;
		try {
			fis = new FileInputStream(file);
			isr = new InputStreamReader(fis);
			scanner = new Scanner(isr);
			String temp = null;
			while (scanner.hasNextLine()) {
				try {
					errorInfoDealBean
							.dealWithLogInfo(temp = scanner.nextLine());
				} catch (Exception e) {
					if (error_back_file_writer == null) {
						error_back_file_writer = getErrorBackFileWriter();
					}
					error_back_file_writer.write(temp);
					error_back_file_writer.write(LINE_SEPARATOR);
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("read log error :", e);
		} finally {
			try {
				if (error_back_file_writer != null) {
					error_back_file_writer.flush();
					error_back_file_writer.close();
				}
			} catch (Exception e) {
			}
			if (scanner != null) {
				try {
					scanner.close();
					scanner = null;
				} catch (Exception e) {
				}
			}
			if (isr != null) {
				try {
					isr.close();
					isr = null;
				} catch (Exception e) {
				}
			}
			if (fis != null) {
				try {
					fis.close();
					fis = null;
				} catch (Exception e) {
				}
			}
		}
	}

	public static UtilForErrorLog init() {
		openDir(errorPath);
		openDir(backUpPath);

		Thread thread = new ThreadForWrite();
		new Thread(thread).start();
		thread = new ThreadForRead();
		new Thread(thread).start();

		if (util == null) {
			util = new UtilForErrorLog();
		}
		return util;
	}

	public static void main(String[] args) {
		UtilForErrorLog.init();
		int i = 1, j = 1;
		while (true) {
			try {
				UtilForErrorLog.writeErrorDate(i + " " + j + " " + 1);
				++i;
				j = j + 2;
				Thread.sleep(200);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void setErrorInfoDealBean(IDealWithLogInfo errorInfoDealBean) {
		this.errorInfoDealBean = errorInfoDealBean;
	}
}
