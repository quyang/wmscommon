package com.xianzaishi.wms.common.action;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.json.JSONUtil;
import org.apache.struts2.json.SerializationParams;

public class ResponseHelper {
    public static void writeToResponse(HttpServletResponse response, String json) throws IOException {
    	boolean noCache = true;
    	String contentType = "application/json";
    	
        JSONUtil.writeJSONToResponse(new SerializationParams(response, "UTF-8", false,
                json, false, false, noCache, 0, 0, false, contentType, null,
                null));
    }
}
