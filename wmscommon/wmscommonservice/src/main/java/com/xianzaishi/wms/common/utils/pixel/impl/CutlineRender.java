package com.xianzaishi.wms.common.utils.pixel.impl;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Random;

import com.xianzaishi.wms.common.utils.pixel.ImageRender;

public class CutlineRender implements ImageRender {
	int height;
	int width;
	public boolean render(Image srcImg) {
		height = srcImg.getHeight(null);
		width = srcImg.getWidth(null);
		
		Graphics g = srcImg.getGraphics();
		cutLines(g);
		g.dispose();
		return true;
	}
	
	private void cutLines(Graphics g){
		g.setColor(Color.WHITE);
		// 绘制干扰线
		int y = 10;
		while (y < height - 5) {
			g.fillRect(0, y, width, 2);
			y += new Random().nextInt(5)+5;
		}
	}
}
