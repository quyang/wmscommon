package com.xianzaishi.wms.common.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class JsonUtils {

	/**
	 * 将JSON字符串解析为Map对象.
	 * @param strJson
	 * @return 转换后的map. 如果参数strJson为空(null或空白),则返回空map.
	 */
	public static final Map<String, Object> jsonStrToMap(String strJson){
		Map<String, Object> map = new HashMap<String, Object>();
		if(strJson != null && strJson.trim().length() > 0){
			JSONObject json = JSONObject.fromObject(strJson);

			for(Iterator<?> it = json.keys(); it.hasNext(); ){
				String key = (String) it.next();
				map.put(key,  json.get(key));
			}
		}
		return map;
	}
	
	/**
	 * 将JSON串转换成指定对象
	 * @param strJson
	 * @param cls
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static final <T extends Object> T jsonStrToBean(String strJson, Class<T> cls) {
		JSONObject json = JSONObject.fromObject(strJson);
		return (T)JSONObject.toBean(json, cls);
	}
	
	/**
	 * 将指定对象的指定属性转换为json字符串格式.
	 * @param obj 要转换的对象
	 * @param includeProperties 要包含的属性
	 * @return
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static String beanToStr(Object obj, String... includeProperties) {
		if(obj == null || includeProperties == null) return null;
		JSONObject jsonObj = JSONObject.fromObject(obj);
		JSONObject dest = new JSONObject();
		for(String prop: includeProperties){
			if(jsonObj.containsKey(prop)){
				dest.put(prop, jsonObj.get(prop));
			}
		}
		return dest.toString();
	}
	
	/**
	 * 将指定对象的所有属性转换为json串格式
	 * @param obj
	 * @return
	 */
	public static String beanToStr(Object obj) {
		if(obj == null) return null;
		if(obj instanceof List 
				|| obj instanceof Collection
				|| obj instanceof Object[]){
			return JSONArray.fromObject(obj).toString();
		}
		return JSONObject.fromObject(obj).toString();
	}
	
}
