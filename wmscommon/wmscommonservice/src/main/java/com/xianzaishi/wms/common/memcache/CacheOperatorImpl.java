package com.xianzaishi.wms.common.memcache;

import java.util.Date;
import java.util.concurrent.ExecutionException;

import net.spy.memcached.MemcachedClient;
import net.spy.memcached.internal.OperationFuture;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class CacheOperatorImpl implements ICacheOperator {

	private static Log log = LogFactory.getLog(CacheOperatorImpl.class);
	@Autowired
	private MemcachedClient spyMencachedClient = null;

	public boolean set(String key, Object value, Date expired) {
		if (log.isDebugEnabled())
			log.debug(" ###[Cache set]### :" + key + "=" + value);
		boolean flag = false;
		try {
			OperationFuture<Boolean> future = spyMencachedClient.set(key,
					new Long(System.currentTimeMillis() - expired.getTime())
							.intValue(), value);
			flag = future.get();
		} catch (InterruptedException e) {
			log.error("cache操作失败:", e);
		} catch (ExecutionException e) {
			log.error("cache操作失败:", e);
		}
		return flag;
	}

	public Object get(String key) {
		if (log.isDebugEnabled())
			log.debug(" ###[Cache get]### :" + key);
		Object flag = null;
		try {
			flag = spyMencachedClient.get(key);
			if (log.isDebugEnabled())
				log.debug(" ###[Cache get]### =" + flag);
		} catch (Exception e) {
			log.error("cache操作失败:", e);
		}
		return flag;
	}

	public Boolean delete(String key) {
		if (log.isDebugEnabled())
			log.debug(" ###[Cache get]### :" + key);
		Boolean flag = null;
		try {
			flag = (Boolean) spyMencachedClient.delete(key).get();
			if (log.isDebugEnabled())
				log.debug(" ###[Cache get]### =" + flag);
		} catch (Exception e) {
			log.error("cache操作失败:", e);
		}
		return flag;
	}

	public boolean set(String key, Object value, Integer expired) {
		if (log.isDebugEnabled())
			log.debug(" ###[Cache set]### :" + key + "=" + value);
		boolean flag = false;
		try {
			OperationFuture<Boolean> future = spyMencachedClient.set(key,
					expired, value);
			flag = future.get();
		} catch (InterruptedException e) {
			log.error("cache操作失败:", e);
		} catch (ExecutionException e) {
			log.error("cache操作失败:", e);
		}
		return flag;
	}

	public boolean add(String key, Object value, Integer expired) {
		if (log.isDebugEnabled())
			log.debug(" ###[Cache set]### :" + key + "=" + value);
		boolean flag = false;
		try {
			OperationFuture<Boolean> future = spyMencachedClient.add(key,
					expired, value);
			flag = future.get();
		} catch (InterruptedException e) {
			log.error("cache操作失败:", e);
		} catch (ExecutionException e) {
			log.error("cache操作失败:", e);
		}
		return flag;
	}

	public boolean add(String key, Object value, Date expired) {
		if (log.isDebugEnabled())
			log.debug(" ###[Cache set]### :" + key + "=" + value);
		boolean flag = false;
		try {
			OperationFuture<Boolean> future = spyMencachedClient.add(key,
					new Long(System.currentTimeMillis() - expired.getTime())
							.intValue(), value);
			flag = future.get();
		} catch (InterruptedException e) {
			log.error("cache操作失败:", e);
		} catch (ExecutionException e) {
			log.error("cache操作失败:", e);
		}
		return flag;
	}

	public MemcachedClient getSpyMencachedClient() {
		return spyMencachedClient;
	}

	public void setSpyMencachedClient(MemcachedClient spyMencachedClient) {
		this.spyMencachedClient = spyMencachedClient;
	}
}
