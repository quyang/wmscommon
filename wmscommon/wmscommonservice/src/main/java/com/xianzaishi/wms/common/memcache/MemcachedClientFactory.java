package com.xianzaishi.wms.common.memcache;

import com.whalin.MemCached.MemCachedClient;

public class MemcachedClientFactory {
	private static MemCachedClient memCachedClient = null;

	public static MemCachedClient getInstance() {
		if (memCachedClient == null) {
			memCachedClient = new MemCachedClient();
			memCachedClient.setPrimitiveAsString(true);
			memCachedClient.setCompressEnable(true);
			memCachedClient.setCompressThreshold(4 * 1024);
		}
		return memCachedClient;
	}
}
