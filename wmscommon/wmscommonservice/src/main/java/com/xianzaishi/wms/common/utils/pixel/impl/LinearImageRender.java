package com.xianzaishi.wms.common.utils.pixel.impl;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.PixelGrabber;
import java.util.Random;

import javax.swing.ImageIcon;

import com.xianzaishi.wms.common.utils.pixel.ImageRender;

public class LinearImageRender implements ImageRender {
	/**
	 * 变形基准参考点位置
	 */
	public enum BasePoint{
		/**随机*/
		RANDOM,
		/**左上角*/
		TOP_LEFT,
		/**上方中点*/
		TOP_CENTER,
		/**右上角*/
		TOP_RIGHT,
		/**左边中点*/
		LEFT_MIDDLE,
		/**右边中点*/
		RIGHT_MIDDLE,
		/**左下角*/
		BOTTOM_LEFT,
		/**下方中点*/
		BOTTOM_CENTER,
		/**右下角*/
		BOTTOM_RIGHT;
	}
	
	BasePoint basePoint = BasePoint.RANDOM;
	int baseX;
	int baseY;
	int w, h; // 图像大小： w为宽，h为高
	int[] temp;// 图像缓存数组
	int[] deal;// 处理后图像数组
	
	
	public LinearImageRender(){}
	public LinearImageRender(BasePoint basePoint){this.basePoint = basePoint;}
	
	public boolean render(Image srcImg) {
		ImageIcon image = new ImageIcon(srcImg);
		w = image.getIconWidth();// 获取图像信息
		h = image.getIconHeight();
		deal = new int[w * h];
		temp = new int[w * h];
		handlepixels(srcImg, 0, 0, w, h);// 抓取图像像素
		transform();// 插值
		temp = null;

		// 应用变换
		if (srcImg instanceof BufferedImage) {
			((BufferedImage) srcImg).setRGB(0, 0, w, h, deal/* 数组 */, 0, w);
		} else {
			BufferedImage bufImage = new BufferedImage(w, h,
					BufferedImage.TYPE_INT_RGB);
			bufImage.setRGB(0, 0, w, h, deal/* 数组 */, 0, w);
			Graphics g2 = srcImg.getGraphics();
			g2.drawImage(bufImage, 0, 0, null);
			g2.dispose();

		}
		deal = null;
		return true;
	}

	private void handlepixels(Image img, int x, int y, int w, int h) // 抓取图像像素
	{
		PixelGrabber pg = new PixelGrabber(img, x, y, w, h, temp, 0, w);
		try {
			pg.grabPixels();
		} catch (InterruptedException e) {
			System.err.println("interrupted waiting for pixels!");
			return;
		}
		if ((pg.getStatus() & ImageObserver.ABORT) != 0) {
			System.err.println("image fetch aborted or errored");
			return;
		}
	}

	private void transform() // 变形
	{
		calcBasePoint();
		int r, x1, y1;// 变换区域半径r，目标坐标点的源点坐标（x1,y1）
		double d, c = 0.05, e = 2500, dp, drag, dragstep;// 目标点离圆心距离d，凹陷变换系数c，扭曲变换系数e，扭曲参数drag
		int maxr = (int) Math.sqrt(w * w + h * h) ;
		r = maxr ;
		int i, j;
		for (j = 0; j < h; j++) { // 两重循环逐个扫描像素点
			for (i = 0; i < w; i++) {
				d = Math.sqrt((j - baseY) * (j - baseY) + (i - baseX)
						* (i - baseX)); // 目标像素点离图像中心点距离
				if (d > r) // 如果在图像处理部分之外则保存原来像素
					deal[j * w + i] = temp[j * w + i];
				else {
					dp = (c * r + d) / (1 + c); // 获取源像素点离中心距离
					drag = Math.asin(Math.abs(baseY - j) / d); // 获取目标像素点和中心点之间的角度
					dragstep = Math.PI * (r - d) / e; // 扭曲参数增量随半径减小而增大
					if ((baseX - i > 0 || baseX - i == 0) && baseY - j > 0) // 分别按四个坐标区域处理目标像素点的值
					{
						//左上
						drag = drag - dragstep; // 扭曲参数变换
						x1 = (int) Math.ceil((baseX - dp * Math.cos(drag))); // 获取源像素点横坐标
						y1 = (int) Math.ceil((baseY - dp * Math.sin(drag))); // 获取源像素点纵坐标
					} else if (baseX - i < 0
							&& (baseY - j > 0 || baseY - j == 0)) { // 同上
						//右上,右平
						drag = drag + dragstep;
						x1 = (int) Math.ceil((baseX + dp * Math.cos(drag)));
						y1 = (int) Math.ceil((baseY - dp * Math.sin(drag)));
					} else if (baseX - i > 0
							&& (baseY - j < 0 || baseY - j == 0)) {
						//左下
						drag = drag + dragstep;
						x1 = (int) Math.ceil((baseX - dp * Math.cos(drag)));
						y1 = (int) Math.ceil((baseY + dp * Math.sin(drag)));
					} else {
						//右下
						drag = drag - dragstep;
						x1 = (int) Math.ceil((baseX + dp * Math.cos(drag)));
						y1 = (int) Math.ceil((baseY + dp * Math.sin(drag)));
					}
					
					deal[j * w + i] = getPoint(x1, y1);//average(getAdjacentPoints(x1, y1)); // 对像素点进行插值 获取目标像素点的值
				}
			}
		}
	}
	
	//计算变形基准点坐标
	private void calcBasePoint() {
		switch (this.basePoint){
		case RANDOM : 
			randomBp();
			calcBasePoint();
			break;
		case TOP_LEFT: baseX = 0; baseY = 0; break;
		case TOP_CENTER: baseX = w/2; baseY = 0; break;
		case TOP_RIGHT: baseX = w; baseY = 0; break;
		case LEFT_MIDDLE: baseX = 0; baseY = h/2; break;
		case RIGHT_MIDDLE: baseX = w; baseY = h/2; break;
		case BOTTOM_LEFT: baseX = 0; baseY = h; break;
		case BOTTOM_CENTER: baseX = w/2; baseY = h; break;
		case BOTTOM_RIGHT: baseX = w; baseY = h; break;
		default:  baseX = w; baseY = h; break;
		}
	}
	
	private void randomBp(){
		int r = new Random().nextInt(BasePoint.values().length) + 1;
		if(r >= BasePoint.values().length) r = BasePoint.values().length - 1;
		basePoint = BasePoint.values()[r];
	}
	
	private int[] getAdjacentPoints(int x, int y){
		int[] p = new int[4];
		if(x < 0) x = 0;
		if(y < 0) y = 0;
		if(x > w - 1) x = w - 1;
		if(y > h - 1) y = h - 1;
		p[0] = getPoint(x, y);
		p[1] = getPoint(x + 1, y);
		p[2] = getPoint(x, y + 1);
		p[3] = getPoint(x + 1, y + 1);
		return p;
	}
	private int getPoint(int x, int y){
		if(x < 0) x = 0;
		if(y < 0) y = 0;
		if(x > w - 1) x = w - 1;
		if(y > h - 1) y = h - 1;
		return temp[y * w + x];
	}

	private int average(int[] pixel) // 对像素点进行插值
	{
		int CHpixel;
		int[] a = new int[4];
		int[] r = new int[4];
		int[] g = new int[4];
		int[] b = new int[4];
		for (int i = 0; i < 4; i++) // 分别获取相邻四个像素点的值
		{
			a[i] = (pixel[i] >> 24) & 0xff;
			r[i] = (pixel[i] >> 16) & 0xff;
			g[i] = (pixel[i] >> 8) & 0xff;
			b[i] = (pixel[i]) & 0xff;

		}
		CHpixel = ((a[0] + a[1] + a[2] + a[3]) / 4) << 24 // 对相邻四个点进行一次平均插值
				| ((r[0] + r[1] + r[2] + r[3]) / 4) << 16
				| ((g[0] + g[1] + g[2] + g[3]) / 4) << 8
				| ((b[0] + b[1] + b[2] + b[3]) / 4);
		return CHpixel;
	}
	
	/**
	 * 设置基准点位置. 默认为随机(BasePoint.RANDOM)
	 * @param basePoint
	 */
	public void setBasePoint(BasePoint basePoint) {
		this.basePoint = basePoint;
	}


}
