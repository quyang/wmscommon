package com.xianzaishi.wms.common.utils.worker;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xianzaishi.wms.common.vo.SimpleResultVO;

public class ExecutorManager {
	private static Log log = LogFactory.getLog(ExecutorManager.class);
	private static ExecutorService threadPool = Executors.newCachedThreadPool();
	private static Long WAIT_TIME = 200l;

	private ExecutorManager() {
	}

	public static void execute(Worker worker) throws Exception {
		threadPool.execute(worker);
	}

	public static SimpleResultVO executeForTime(Worker worker) throws Exception {
		SimpleResultVO flag = new SimpleResultVO();
		try {
			synchronized (worker.getParentLock()) {
				execute(worker);
				worker.getParentLock().wait(worker.getExecuteTime());
				if (worker.isSuccess()) {
					flag.setTarget(worker.getResult());
					flag.setReturn_code(SimpleResultVO.RETURN_SUCCESS);
				}
			}
		} catch (Exception e) {
			log.error("execute for time error single : ", e);
			flag.setReturn_code(SimpleResultVO.RETURN_ERROR);
			flag.setMessage(e.getMessage());
		}
		return flag;
	}

	/**
	 * worker原子性
	 */
	public static SimpleResultVO executeForTimeAtomicity(
			List<WorkerForMutilAtomicity> workers) throws Exception {
		SimpleResultVO flag = new SimpleResultVO();
		List<Object> temp = null;
		try {
			AtomicInteger counter = new AtomicInteger(0);
			synchronized (counter) {
				for (WorkerForMutilAtomicity worker : workers) {
					worker.setCounter(counter);
					worker.setTotal(workers.size());
					execute(worker);
				}
				counter.wait(workers.isEmpty() ? WAIT_TIME : workers.get(0)
						.getExecuteTime());
				if (counter.get() == workers.size()) {
					temp = new LinkedList<Object>();
					for (WorkerForMutilAtomicity worker : workers) {
						if (worker.isSuccess()) {
							temp.add(worker.getResult());
						} else {
							throw new Exception("execute for time error .");
						}
					}
					flag.setTarget(temp);
					flag.setReturn_code(SimpleResultVO.RETURN_SUCCESS);
				} else {
					flag.setMessage("mutil worker excute error!");
					flag.setReturn_code(SimpleResultVO.RETURN_ERROR);
				}
			}
		} catch (Exception e) {
			log.error("execute for time error mutil : ", e);
			flag.setReturn_code(SimpleResultVO.RETURN_ERROR);
			flag.setMessage(e.getMessage());
		}
		return flag;
	}

	/**
	 * worker非原子性
	 */
	public static SimpleResultVO executeForTime(List<WorkerForMutil> workers)
			throws Exception {
		SimpleResultVO flag = new SimpleResultVO();
		List<Object> temp = null;
		try {
			AtomicInteger counter = new AtomicInteger(0);
			synchronized (counter) {
				for (WorkerForMutil worker : workers) {
					worker.setCounter(counter);
					worker.setTotal(workers.size());
					execute(worker);
				}
				counter.wait(workers.isEmpty() ? WAIT_TIME : workers.get(0)
						.getExecuteTime());
				temp = new LinkedList<Object>();
				for (WorkerForMutil worker : workers) {
					if (worker.isSuccess()) {
						temp.add(worker.getResult());
					}
				}
				flag.setTarget(temp);
				flag.setReturn_code(SimpleResultVO.RETURN_SUCCESS);
			}
		} catch (Exception e) {
			log.error("execute for time error mutil : ", e);
			flag.setReturn_code(SimpleResultVO.RETURN_ERROR);
			flag.setMessage(e.getMessage());
		}
		return flag;
	}
}
