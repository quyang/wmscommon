package com.xianzaishi.wms.common.utils.pixel;

import java.awt.Image;

/**
 * 图像渲染接口
 * @author lirui
 *
 */
public interface ImageRender {
	
	public boolean render(Image srcImg);
}
