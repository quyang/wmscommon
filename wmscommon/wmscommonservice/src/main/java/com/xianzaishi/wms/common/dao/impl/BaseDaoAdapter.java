package com.xianzaishi.wms.common.dao.impl;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.orm.ibatis.SqlMapClientTemplate;

import com.ibatis.sqlmap.client.SqlMapExecutor;
import com.xianzaishi.wms.common.dao.itf.IBaseDao;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.BaseVO;
import com.xianzaishi.wms.common.vo.QueryVO;

public abstract class BaseDaoAdapter<T extends BaseVO, M extends QueryVO>
		implements IBaseDao<T, M> {
	protected SqlMapClientTemplate simpleSqlMapClientTemplate = null;
	private final Log log = LogFactory.getLog(BaseDaoAdapter.class);

	// 添加对象
	public Object addDO(T baseDO) {
		try {
			Object pk = simpleSqlMapClientTemplate.insert(getVOClassName()
					+ ".add", baseDO);
			return pk;
		} catch (Exception e) {
			log.error("添加数据失败。", e);
			throw new BizException("添加数据失败。", e);
		}
	}

	// 修改对象
	public Boolean updateDO(T baseDO) {
		Boolean flag = false;
		try {
			if (baseDO.getId() != null && baseDO.getId() > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".update", baseDO);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("数据不存在。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + baseDO.getId());
			}
		} catch (Exception e) {
			log.error("更新数据失败。", e);
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}

	// 根据ID获取对象
	public T getDOByID(Long id) {
		return getDOByID(id, getVOClassName());
	}

	// 根据ID获取对象
	public T getDOByID(Long id, String className) {
		T flag = null;
		try {
			if (id != null && id > 0) {
				flag = (T) simpleSqlMapClientTemplate.queryForObject(className
						+ ".getDOByID", id);
			} else {
				throw new BizException("获取数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			log.error("获取数据失败。", e);
			throw new BizException("获取数据失败。", e);
		}
		return flag;
	}

	// 查询对象
	public List<T> queryDO(M queryVO) {
		return queryDO(queryVO, getVOClassName());
	}

	@SuppressWarnings("unchecked")
	public List<T> queryDO(String sqlId, Map<String, Object> conditions) {
		List<T> flag = new LinkedList<T>();
		try {
			flag = (List<T>) simpleSqlMapClientTemplate.queryForList(
					getVOClassName() + "." + sqlId, conditions);
		} catch (Exception e) {
			log.error("查询数据失败。", e);
			throw new BizException("查询数据失败。", e);
		}
		return flag;
	}

	// 查询对象
	public List<T> queryDO(M queryVO, String className) {
		List<T> flag = new LinkedList<T>();
		try {
			flag = (List<T>) simpleSqlMapClientTemplate.queryForList(className
					+ ".query", queryVO);
		} catch (Exception e) {
			log.error("查询数据失败。", e);
			throw new BizException("查询数据失败。", e);
		}
		return flag;
	}

	public Integer queryCount(M queryVO) {
		return queryCount(queryVO, getVOClassName());
	}

	// 查询对象总数
	public Integer queryCount(M queryVO, String className) {
		Integer flag = null;
		try {
			flag = (Integer) simpleSqlMapClientTemplate.queryForObject(
					className + ".queryCount", queryVO);
		} catch (Exception e) {
			log.error("查询数据失败。", e);
			throw new BizException("查询数据失败。", e);
		}
		return flag;
	}

	// 自定义 sqlmap 中的查询条件ID
	public Integer queryCount(M queryVO, String className, String sqlId) {
		Integer flag = null;
		try {
			flag = (Integer) simpleSqlMapClientTemplate.queryForObject(
					className + "." + sqlId, queryVO);
		} catch (Exception e) {
			log.error("查询数据失败。", e);
			throw new BizException("查询数据失败。", e);
		}
		return flag;
	}

	// 物理删除对象
	public Boolean deleteDO(T baseDO) {
		Boolean flag = false;
		try {
			if (baseDO.getId() != null && baseDO.getId() > 0) {
				int no = simpleSqlMapClientTemplate.delete(baseDO.getClass()
						.getSimpleName() + ".delete", baseDO);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("数据不存在。");
				}
			} else {
				throw new BizException("删除数据失败，ID错误：ID = " + baseDO.getId());
			}
		} catch (Exception e) {
			log.error("删除数据失败。", e);
			throw new BizException("删除数据失败。", e);
		}
		return flag;
	}

	// 物理删除对象
	public Boolean deleteDO(Long id, String className) {
		Boolean flag = false;
		try {
			if (id != null && id > 0) {
				int no = simpleSqlMapClientTemplate.delete(className
						+ ".deleteByID", id);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("数据不存在。");
				}
			} else {
				throw new BizException("删除数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			log.error("删除数据失败。", e);
			throw new BizException("删除数据失败。", e);
		}
		return flag;
	}

	// 物理删除对象
	public Boolean deleteDO(Long id) {
		if (StringUtils.isEmpty(getVOClassName())) {
			throw new BizException("请指定VO类型。");
		}
		return deleteDO(id, getVOClassName());
	}

	// 逻辑删除对象
	public Boolean delDO(T baseDO) {
		Boolean flag = false;
		try {
			if (baseDO.getId() != null && baseDO.getId() > 0) {
				int no = simpleSqlMapClientTemplate.update(baseDO.getClass()
						.getSimpleName() + ".del", baseDO);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("数据不存在。");
				}
			} else {
				throw new BizException("删除数据失败，ID错误：ID = " + baseDO.getId());
			}
		} catch (Exception e) {
			log.error("删除数据失败。", e);
			throw new BizException("删除数据失败。", e);
		}
		return flag;
	}

	// 逻辑删除对象
	public Boolean delDO(Long id) {
		if (StringUtils.isEmpty(getVOClassName())) {
			throw new BizException("请指定VO类型。");
		}
		return delDO(id, getVOClassName());
	}

	/**
	 * 批量逻辑删除
	 */
	public Boolean delDOBatch(final Long... id) {
		Integer c = (Integer) simpleSqlMapClientTemplate
				.execute(new SqlMapClientCallback() {

					public Object doInSqlMapClient(SqlMapExecutor executor)
							throws SQLException {
						executor.startBatch();
						for (Long i : id) {
							executor.update(getVOClassName() + ".delByID", i);
						}
						return executor.executeBatch();
					}
				});
		return c != null && c > 0;
	}

	// 逻辑删除对象
	public Boolean delDO(Long id, String className) {
		Boolean flag = false;
		try {
			if (id != null && id > 0) {
				int no = simpleSqlMapClientTemplate.update(className
						+ ".delByID", id);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("数据不存在。");
				}
			} else {
				throw new BizException("删除数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			log.error("删除数据失败。", e);
			throw new BizException("删除数据失败。", e);
		}
		return flag;
	}

	/**
	 * 批量更新DR标记.由参数queryvo限制更新范围.
	 * 
	 * @param dr
	 *            - 要更新的DR字段值
	 * @param baseDO
	 *            - 限制数据更新范围. 如果为null,抛出BizException异常.
	 * @return 影响的数据行数.
	 * @exception IllegalArgumentException
	 *                当参数queryvo为null时.
	 */
	public int batchUpdateDr(short dr, T baseDO) {
		if (baseDO == null)
			throw new IllegalArgumentException("baseDO为空.");
		baseDO.setDr(dr);
		return simpleSqlMapClientTemplate.update(getVOClassName()
				+ ".batchUpdateDr", baseDO);
	}

	public SqlMapClientTemplate getSimpleSqlMapClientTemplate() {
		return simpleSqlMapClientTemplate;
	}

	public String getVOClassName() {
		return null;
	}

	public void setSimpleSqlMapClientTemplate(
			SqlMapClientTemplate simpleSqlMapClientTemplate) {
		this.simpleSqlMapClientTemplate = simpleSqlMapClientTemplate;
	}

	public Boolean batchAddDO(List<T> baseDOs) {
		for (int i = 0; i < baseDOs.size(); i++) {
			addDO(baseDOs.get(i));
		}
		return true;
	}

	public Boolean batchModifyDO(List<T> baseDOs) {
		for (int i = 0; i < baseDOs.size(); i++) {
			updateDO(baseDOs.get(i));
		}
		return true;
	}

	public Boolean batchDeleteDO(List<T> baseDOs) {
		for (int i = 0; i < baseDOs.size(); i++) {
			delDO(baseDOs.get(i));
		}
		return true;
	}

	public Boolean batchDeleteDOByID(List<Long> ids) {
		for (int i = 0; i < ids.size(); i++) {
			delDO(ids.get(i));
		}
		return true;
	}

	public Boolean batchDeleteDOPhysics(List<T> baseDOs) {
		for (int i = 0; i < baseDOs.size(); i++) {
			deleteDO(baseDOs.get(i));
		}
		return true;
	}

	public Boolean batchDeleteDOByIDPhysics(List<Long> ids) {
		for (int i = 0; i < ids.size(); i++) {
			deleteDO(ids.get(i));
		}
		return true;
	}
}
