package com.xianzaishi.wms.common.utils.file.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xianzaishi.wms.common.utils.file.IRemoteFileHandler;

/**
 * 本地磁盘文件处理器.
 * <p>
 * 将文件保存到本地磁盘.开发调试使用.
 * 
 * @author lirui
 * 
 */
public class LocalDiskFileHandler implements IRemoteFileHandler {
	private static final Log log = LogFactory
			.getLog(LocalDiskFileHandler.class);

	public void config(Map<String, String> config) {
		// do nothing for local
	}

	public String saveFile(InputStream in, String baseDir, String subdir,
			String filename) throws IOException {
		String fileDirPath = getFileDirPath(baseDir, subdir);
		prepareDir(fileDirPath);
		try {
			File destFile = createDestFile(fileDirPath, filename);
			copyFile(in, destFile);
			return destFile.getName();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}

	private void copyFile(InputStream in, File dest) throws IOException {
		OutputStream out = null;
		byte[] buffer = new byte[4096];

		try {
			out = new FileOutputStream(dest);
			int s = 0;
			while ((s = in.read(buffer)) > -1) {
				out.write(buffer, 0, s);
			}
		} finally {
			try {
				in.close();
			} catch (Exception e) {
			}
			try {
				out.close();
			} catch (Exception e) {
			}
		}

	}

	private void prepareDir(String dir) {
		File baseDir = new File(dir);
		if (!baseDir.exists()) {
			baseDir.mkdirs();
		}
		baseDir = null;
	}

	private File createDestFile(String dirPath, String filename)
			throws IOException {
		File f = new File(dirPath + File.separator + filename);
		int i = 2;
		while (!f.createNewFile()) {
			f = new File(dirPath + File.separator + i + "_" + filename);
			i++;
		}

		return f;
	}

	private String getFileDirPath(String basedir, String subdir) {
		return basedir + File.separator + subdir;
	}

	public String getFile(String inFileName, String baseDir, String subdir,
			String filename) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	public void disconnect() throws IOException {

	}

	public InputStream getInputStream(String fileName) throws IOException {
		InputStream is = null;
		File file = new File(fileName);
		if (file != null) {
			is = new FileInputStream(file);
		}
		return is;
	}

	public OutputStream getOutputStream(String fileName) throws IOException {
		OutputStream os = null;
		File file = new File(fileName);
		if (file != null) {
			os = new FileOutputStream(file);
		}
		return os;
	}

	public boolean connect() {
		// TODO Auto-generated method stub
		return false;
	}
}
