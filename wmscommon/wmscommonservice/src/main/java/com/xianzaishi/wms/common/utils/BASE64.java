package com.xianzaishi.wms.common.utils;

import org.apache.commons.codec.binary.Base64;


/**
 * base64 编码解码助手
 * @author lirui
 *
 */
public class BASE64 {
	/**
	 * 将指定字符串以BASE64编码
	 * @param s
	 * @return
	 */
	public static String encode(String s) {
		if(s == null) return null;
		byte[] digest = Base64.encodeBase64(s.getBytes());
		return new String(digest);
	}
	
	/**
	 * 将指定字符串以BASE64解码
	 * @param s
	 * @return
	 */
	public static String decode(String s){
		if(s == null) return null;
		byte[] val = Base64.decodeBase64(s);
		return new String(val);
	}
	
	public static void main(String[] s){
			System.out.println(BASE64.encode("XianzAishI168"));
			System.out.println(BASE64.decode("WGlhbnpBaXNoSTE2OA=="));
			System.out.println(BASE64.decode("cm9vdA=="));
			System.out.println(BASE64.encode("nfgc0db0P"));
	}
}
