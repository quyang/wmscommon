package com.xianzaishi.wms.common.utils;

import com.xianzaishi.wms.common.exception.BizException;

/**
 * Created by quyang on 2017/5/6.
 */
public class PageUtils {


  /**
   * 获取总页数
   */
  public static Integer getTotalPage(Integer totalCount, Integer pageSize) {

    if (null == totalCount || null == pageSize || pageSize == 0) {
      throw new BizException("总页数或页面大小值错误");
    }

    int i = totalCount / pageSize;
    int yushu = totalCount % pageSize;
    if (yushu != 0) {
      i = i + 1;
    }
    return i;
  }

}
