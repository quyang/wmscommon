package com.xianzaishi.wms.common.spymemcache;

import java.io.IOException;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.ConnectionFactoryBuilder;
import net.spy.memcached.ConnectionFactoryBuilder.Protocol;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.auth.AuthDescriptor;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.schooner.MemCached.PlainCallbackHandler;
import com.xianzaishi.wms.common.utils.BASE64;

public class SpyMemcachedClientFactory {

	private static final Log log = LogFactory
			.getLog(SpyMemcachedClientFactory.class);

	private static MemcachedClient memcachedClient = null;

	private static String[] plain = new String[] { "PLAIN" };

	private static String account = null;

	private static String password = null;

	private static String servers = null;

	private static Long timeOut = 100l;

	private SpyMemcachedClientFactory() {
	}

	public MemcachedClient getInstance() {
		try {
			if (memcachedClient == null) {
				if (servers != null && !StringUtils.isEmpty(servers)) {

					ConnectionFactoryBuilder builder = new ConnectionFactoryBuilder()
							.setProtocol(Protocol.BINARY) // 指定使用Binary协议
							.setOpTimeout(timeOut);// 设置超时时间为100ms

					if (account != null && account.length() > 0) {
						AuthDescriptor ad = new AuthDescriptor(plain,
								new PlainCallbackHandler(account, password)); // 用户名，密码
						builder.setAuthDescriptor(ad);
					}

					memcachedClient = new MemcachedClient(builder.build(),
							AddrUtil.getAddresses(servers)); // 访问地址
				}
			}
		} catch (IOException e) {
			log.error("初始化memcachedclient失败：", e);
		}
		return memcachedClient;
	}

	public static void main(String[] args) {
		System.out.println(BASE64.encode("lovey0cache0P"));
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		SpyMemcachedClientFactory.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		SpyMemcachedClientFactory.password = password;
	}

	public Long getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(Long timeOut) {
		SpyMemcachedClientFactory.timeOut = timeOut;
	}

	public String getServers() {
		return servers;
	}

	public void setServers(String servers) {
		SpyMemcachedClientFactory.servers = servers;
	}
}
