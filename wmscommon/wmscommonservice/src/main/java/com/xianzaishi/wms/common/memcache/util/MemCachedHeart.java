package com.xianzaishi.wms.common.memcache.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.memcache.ICacheOperator;

public class MemCachedHeart extends Thread {

	private static Log log = LogFactory.getLog(MemCachedHeart.class);

	@Autowired(required = true)
	private ICacheOperator cacheOperator = null;

	private MemCachedHeart() {

	}

	public void init() {
		new Thread(new MemCacheHeartThread()).start();
	}

	private class MemCacheHeartThread extends Thread {
		public void run() {
			while (true) {
				try {
					cacheOperator.get("0");
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
				try {
					Thread.sleep(5000);
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
		}
	}

	public ICacheOperator getCacheOperator() {
		return cacheOperator;
	}

	public void setCacheOperator(ICacheOperator cacheOperator) {
		this.cacheOperator = cacheOperator;
	}
}
