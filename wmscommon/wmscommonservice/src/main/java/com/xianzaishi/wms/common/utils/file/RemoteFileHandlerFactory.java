package com.xianzaishi.wms.common.utils.file;

import java.io.InputStreamReader;
import java.util.Properties;

import com.xianzaishi.wms.common.utils.BASE64;
import com.xianzaishi.wms.common.utils.StringUtils;
import com.xianzaishi.wms.common.utils.file.impl.FtpFileHandler;
import com.xianzaishi.wms.common.utils.file.impl.LocalDiskFileHandler;

/**
 * 文件处理器工厂类.获取文件处理器实例.
 * 
 * <pre>
 * RemoteFileHandlerFactory.getInstance().getFileHandle("ftp")获取FTP文件处理器
 * 或者
 * RemoteFileHandlerFactory.getInstance().getFileHandle("local")获取本地磁盘文件处理器
 * </pre>
 * 
 * @author lirui
 * 
 */
public class RemoteFileHandlerFactory {
	private static final RemoteFileHandlerFactory factory = new RemoteFileHandlerFactory();

	public final static String FTP_SERVER = "FTP_SERVER";
	public final static String FTP_PORT = "FTP_PORT";
	public final static String FTP_USER = "FTP_USER";
	public final static String FTP_PASSWORD = "FTP_PASSWORD";

	public final static String LOCAL = "local";

	public final static String FTP = "ftp";

	// private static Map<String, IRemoteFileHandler> cache = new
	// HashMap<String, IRemoteFileHandler>();
	private RemoteFileHandlerFactory() {
	}

	public static final RemoteFileHandlerFactory getInstance() {
		return factory;
	}

	// public IRemoteFileHandler getFileHandle(String handleName) throws
	// Exception {
	/*
	 * IRemoteFileHandler handler = cache.get(handleName); if(handler == null){
	 * handler = cache.put(handleName, createHandler(handleName)); handler =
	 * cache.get(handleName); }
	 */
	// return createHandler(handleName);
	// }

	public IRemoteFileHandler createHandler(String handleName) throws Exception {
		/*
		 * IRemoteFileHandler handler = cache.get(handleName); if(handler ==
		 * null){ handler = cache.put(handleName, createHandler(handleName));
		 * handler = cache.get(handleName); }
		 */
		return createHandler(handleName, null);
	}

	private IRemoteFileHandler createHandler(String handleName, String filePath)
			throws Exception {
		IRemoteFileHandler fileHandler = null;
		if (LOCAL.equals(handleName)) {
			fileHandler = new LocalDiskFileHandler();
		} else if (FTP.equals(handleName)) {
			if (filePath == null) {
				filePath = "file/fileServer.properties";
			}
			Properties properties = new Properties();
			properties.load(new InputStreamReader(
					RemoteFileHandlerFactory.class.getClassLoader()
							.getResourceAsStream(filePath)));
			FtpFileHandler tempFileHandler = new FtpFileHandler();
			String temp = (String) properties
					.get(RemoteFileHandlerFactory.FTP_SERVER);
			if (temp != null && !StringUtils.isEmpty(temp)) {
				tempFileHandler.setServer(temp);
			}
			temp = (String) properties.get(RemoteFileHandlerFactory.FTP_PORT);
			if (temp != null && !StringUtils.isEmpty(temp)) {
				tempFileHandler.setPort(Integer.parseInt(temp));
			}
			temp = (String) properties.get(RemoteFileHandlerFactory.FTP_USER);
			if (temp != null && !StringUtils.isEmpty(temp)) {
				tempFileHandler.setUsername(temp);
			}
			temp = (String) properties
					.get(RemoteFileHandlerFactory.FTP_PASSWORD);
			if (temp != null && !StringUtils.isEmpty(temp)) {
				tempFileHandler.setPassword(BASE64.decode(temp));
			}
			fileHandler = tempFileHandler;
		}
		return fileHandler;
	}
}
