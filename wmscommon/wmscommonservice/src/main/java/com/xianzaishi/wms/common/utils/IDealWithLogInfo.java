package com.xianzaishi.wms.common.utils;

public interface IDealWithLogInfo {

	public void dealWithLogInfo(String logInfo) throws Exception;

}
