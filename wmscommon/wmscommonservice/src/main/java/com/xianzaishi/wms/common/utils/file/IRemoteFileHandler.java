package com.xianzaishi.wms.common.utils.file;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface IRemoteFileHandler {
	
	/**
	 * 保存文件到指定的子目录.<strong>此操作会关闭输入流.</strong><p>
	 * 父目录由系统配置文件指定或约定.
	 * @param in 要处理的文件
	 * @param baseDir 目标目录.可以是多级目录(/分隔).如果不存在,则会尝试创建.
	 * @param subdir 目标子目录名称.可以是多级目录(/分隔).如果不存在,则会尝试创建.
	 * @param filename 目标文件名称.如果文件已存在,则会尝试重新命名.
	 * @return 文件名称.
	 */
	public String saveFile(InputStream in, String baseDir, String subdir, String filename)
	throws IOException;
	
	public String getFile(String inFileName, String baseDir, String subdir, String filename)
	throws IOException;
	
	public boolean connect();
	
	public void disconnect()throws IOException;
	
	public InputStream getInputStream(String fileName) throws IOException ;
	
	public OutputStream getOutputStream(String fileName) throws IOException ;
	
}
