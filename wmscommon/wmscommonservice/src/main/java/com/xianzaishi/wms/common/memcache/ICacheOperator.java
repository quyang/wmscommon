package com.xianzaishi.wms.common.memcache;

import java.util.Date;

public interface ICacheOperator {

	public boolean set(String key, Object value, Date expired);

	public Object get(String key);

	public boolean set(String key, Object value, Integer expired);

	public boolean add(String key, Object value, Integer expired);

	public boolean add(String key, Object value, Date expired);

	public Boolean delete(String key);
}
