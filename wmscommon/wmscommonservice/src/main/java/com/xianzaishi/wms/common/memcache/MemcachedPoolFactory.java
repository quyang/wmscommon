package com.xianzaishi.wms.common.memcache;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class MemcachedPoolFactory {
	private String servers = null;
	private Integer port = 11211;
	private Integer weight = 1;

	public void init() throws MemcachedException {
		if (servers == null || StringUtils.isEmpty(servers.trim())) {
			return;
		}
		String[] temps = servers.split(",");
		List<MemcachedServer> lists = new LinkedList<MemcachedServer>();
		MemcachedServer memcachedServer = null;
		for (int i = 0; i < temps.length; i++) {
			memcachedServer = new MemcachedServer(temps[i], port, weight);
			lists.add(memcachedServer);
		}
		MemcachedPool pool = MemcachedPool.getInstance();
		pool.initPool(lists);
	}

	public String getServers() {
		return servers;
	}

	public void setServers(String servers) {
		this.servers = servers;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}
}
