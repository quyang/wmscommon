package com.xianzaishi.wms.common.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import net.sf.json.JSONObject;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 处理请求的父类
 * @author Administrator
 *
 */
public class ActionAdapter extends BaseAction {

	public static final String JSON = "json";

	/**
	 * 格式化请求
	 * @param clazz
	 * @return
	 */
	protected SimpleRequestVO formatRequest(Class clazz) {
		Map<String, Class> classMap = new HashMap<String, Class>();
		classMap.put("data", clazz);
		return formatRequest(classMap);
	}
	
	/**
	 * 获取请求对象，所有请求都要调用这个方法
	 * @return
	 */
	protected SimpleRequestVO formatRequest() {
		ActionContext context = ActionContext.getContext();
		HttpServletRequest request = (HttpServletRequest) context
				.get(ServletActionContext.HTTP_REQUEST);
		JSONObject tmp = (JSONObject) JSONObject.fromObject(request
				.getParameter("request"));
		SimpleRequestVO simpleRequestVO = (SimpleRequestVO) JSONObject.toBean(
				tmp, SimpleRequestVO.class);
		return simpleRequestVO;
	}

	protected SimpleRequestVO formatRequest(Map<String, Class> classMap) {
		ActionContext context = ActionContext.getContext();
		HttpServletRequest request = (HttpServletRequest) context
				.get(ServletActionContext.HTTP_REQUEST);
		JSONObject tmp = (JSONObject) JSONObject.fromObject(request
				.getParameter("request"));
		SimpleRequestVO simpleRequestVO = (SimpleRequestVO) JSONObject.toBean(
				tmp, SimpleRequestVO.class, classMap);
		return simpleRequestVO;
	}

	protected void pushResult(Object object) {
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		valueStack.set("result", object);
	}

}
