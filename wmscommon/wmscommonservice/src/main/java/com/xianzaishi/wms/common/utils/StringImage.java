package com.xianzaishi.wms.common.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import com.xianzaishi.wms.common.utils.pixel.ImageRender;

/**
 * 字符串图片类. 以给定字符串生成JPG图片.
 * @author lirui
 *
 */
public class StringImage {
	boolean debug = false;
	private Random random = new Random();
	BufferedImage image;
	private int width ;// 图片宽
	private int height ;// 图片高
	int overwidth = 4;//字符水平重合像素值
	
	String content;
	Color color;
	Font font;
	int charActualHeight;
	
	public StringImage(String str, int width, int height){
		content = str;
		color = randomColor();
		this.width = width;
		this.height = height;
		font = new Font("Dialog", Font.ITALIC, height > 5? height - 5 : 10);
	}
	
	public StringImage(String str){
		this(str, 80, 26);
	}

	/*
	 * 获得颜色
	 */
	private Color randomColor() {
		switch (random.nextInt(3)){
		case 0: return Color.LIGHT_GRAY;
		case 1: return new Color(0xff, 0x44, 0x0);  //橙
		case 2: return new Color(0x0, 0x92, 0xd2); //青
		default: return Color.DARK_GRAY;
		}
	}

	/**
	 * 生成图片
	 */
	public void create() {
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) image.getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		//drawLines(g);
		g.setFont(font);
		g.setColor(color);
		
		FontMetrics fm = g.getFontMetrics();

		//获取字符宽度和水平位置
		int x = 0;
		int strwidth = (int)fm.getStringBounds(content, g).getWidth();//计算整个字符串宽度
		strwidth -= (content.length() - 1)*overwidth;
		x = (width - strwidth)/2;
		x = x < 0?0:x;
		if(debug){//左右边界
			g.drawLine(x, 0, x, height);
			g.drawLine(x+strwidth, 0, x+strwidth, height);
		}
		//获取字符实际高度(约等于font的size)和垂直位置
		charActualHeight = fm.getAscent() - fm.getDescent();
		int y = charActualHeight + (height - charActualHeight)/2 ;
		if(debug){//上下边界
			System.out.println(fm.getHeight()+":descent="+fm.getDescent()+":ascent="+fm.getAscent()+":leading="+fm.getLeading());
			g.drawLine(0, y, width, y);
			g.drawLine(0, y-charActualHeight, width, y-charActualHeight);
		}
		g.setFont(font);
		// 绘制字符
		for (int i = 0; i < content.length(); i++) {
			String nextChar = String.valueOf(content.charAt(i));
			//字符宽度
			Rectangle2D strBounds = fm.getStringBounds(nextChar, g);
			
			drawString(g, nextChar, x, y);
			x += strBounds.getWidth() - overwidth;
		}
		g.dispose();
	}

	/**
	 * 应用一系列渲染器
	 * @param chain
	 */
	public void render(List<ImageRender> chain){
		for(ImageRender warp : chain){
			render(warp);
		}
	}
	
	/**
	 * 应用渲染器
	 * @param render
	 */
	public void render(ImageRender render){
		render.render(image);
	}
	
	/**
	 * 输出图片
	 * @param out
	 */
	public void output(OutputStream out) {
		try {
			ImageWriter writer = null;
			ImageTypeSpecifier type = ImageTypeSpecifier.createFromRenderedImage(image);
			Iterator<ImageWriter> iter = ImageIO.getImageWriters(type, "jpg");
			if (iter.hasNext()) {
				writer = iter.next();
			}
			if (writer == null) {
				return;
			}
			IIOImage iioImage = new IIOImage(image, null, null);
			ImageWriteParam param = writer.getDefaultWriteParam();
			param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			param.setCompressionQuality(1);
			ImageOutputStream outputStream =  ImageIO.createImageOutputStream(out);
			writer.setOutput(outputStream);
			writer.write(null, iioImage, param);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * 绘制字符串
	 */
	private void drawString(Graphics g, String str, int x, int y) {
		g.drawString(str, x, y);
	}

	/**
	 * 获得当前字符颜色
	 * @return
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * 获得当前字体
	 * @return
	 */
	public Font getFont() {
		return font;
	}

	/**
	 * 设置是否调试状态(默认false)
	 * @param debug
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	
	/*//使用方法
	public static void main(String[] test) throws IOException{
		File f = new File("c:\\test.jpg");
		if(!f.exists()) f.createNewFile();
		OutputStream os = new FileOutputStream(f);
		try {
			String s = RandomStringUtils.random(4, '0', 'z', true, true);
			StringImage ri = new StringImage(s,150, 36);
			ri.setFont(new Font("Dialog", Font.ITALIC, 32));
			long t = System.currentTimeMillis();
			ri.create();
			ri.render(new SineImageRender());//正弦波变换
			ri.render(new LinearImageRender());//线性变换
//			ri.render(new CutlineRender());//抽丝
			
			System.out.println("Cost(ms):"+(System.currentTimeMillis() - t));
			ri.output(os);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			os.close();
		}
	}
	*/
}