package com.xianzaishi.wms.common.utils;

/**
 * Created by quyang on 2017/5/7.
 */
public class WeiUtils {

  /**
   * 给数字src的第n位打一个tag = 1
   *
   * @param src 目标数字
   * @param n 第n位
   * @return 返回一个完整的数字
   */
  public static int setTagToOne(int src, int n) {

    if (!isOne(src, n)) {//第n位不是1
      // 让flag的第n位变成1，（二进制表示的最低位是第0位）
      int mask = 1 << n;
      return mask | src;
    }
    return src;
  }


  /**
   * 给数字src的第n位打一个tag = 0
   *
   * @param src 目标数字
   * @param n 第n位
   * @return 返回一个完整的数字
   */
  public static int setTagToZero(int src, int n) {

    if (isOne(src, n)) {//第n位不是1
      // 让src的第n位变成1，（二进制表示的最低位是第0位）
      int mask = 1 << n;

      // 让src的第n位变成0
      src = src | mask;
      mask = ~mask;
      src = src & mask;
      return src;
    }
    return src;
  }

  /**
   * 判断一个数字的第n位是不是1
   */
  public static boolean isOne(int src, int n) {
    int pow = pow(n);
    return ((src & pow) == pow);
  }

  public static int pow(int n) {
    int i = new Double(Math.pow(2, n)).intValue();
    return i;
  }


  /**
   * 增加或者删除src的标
   *
   * @param src 要修改的数字
   * @param tag 标
   * @param add true表示增加标，false表示删除表
   * @return 修改标后的数字
   */
  public static long updateTag(long src, long tag, Boolean add) {
    ObjectUtils.isNull(add, "add参数不能为空");
    if (add) {
      return src | tag;
    } else {
      return src & (~tag);
    }
  }
}
