package com.xianzaishi.wms.common.utils;

import com.xianzaishi.wms.common.exception.BizException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**   
 * @Title: DateUtils.java 
 * @Package com.loveyfruit.util 
 * @author WangShanShan 
 * @date 2013-11-28 下午8:52:57 
 * @version V1.0  
 * 
 *   公共日期时间操作工具类 
 * 
 */

public class DateUtils {

	/** DateTimeFormat1 = "yyyy-MM-dd HH:mm:ss" */
	public static final String DateTimeFormat1 = "yyyy-MM-dd HH:mm:ss";

	/** DateTimeFormat2 = "yyyy-MM-dd HH:mm:ss:SSS" */
	public static final String DateTimeFormat2 = "yyyy-MM-dd HH:mm:ss:SSS";

	/** DateFormat1 = "yyyy-MM-dd" */
	public static final String DateFormat1 = "yyyy-MM-dd";

	/** DateFormat2 = "yyyy/MM/dd" */
	public static final String DateFormat2 = "yyyy/MM/dd";

	/** DateFormat3 = "yyyy.MM.dd" */
	public static final String DateFormat3 = "yyyy.MM.dd";

	/** TimeFormat1 = "HH:mm:ss" */
	public static final String TimeFormat1 = "HH:mm:ss";

	/** TimeFormat2 = "HH:mm:ss:SSS" */
	public static final String TimeFormat2 = "HH:mm:ss:SSS";

	/** _HH : 一小时的毫秒数 60 * 60 * 1000 */
	public static final int _HH = 60 * 60 * 1000;

	/** _MM : 一分钟的毫秒数 60 * 1000 */
	public static final int _MM = 60 * 1000;

	/** _SS : 一秒的毫秒数 1000 */
	public static final int _SS = 1000;

	/**
	 * @return 格式化后的时间字符串。
	 * @param format
	 *            "yyyy-MM-dd HH:mm:ss:SSS"或"yyyy-MM-dd HH:mm:ss"。
	 */
	public static String dateTimeParse(Date date, String format) {
		String flag=null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			flag = sdf.format(date);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return flag;
	}
	
	/**
	 * @return 時間。
	 * @param format。
	 */
	public static Date parse(String sdate, String format) {
		Date flag=null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			flag = sdf.parse(sdate);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return flag;
	}
	
	/**
	 * @return 返回文本信息的日期对应的格林威治标准时间（1970年1月1日00:00:00.000）的偏移量,单位是毫秒。 1秒=1000毫秒。
	 * @param format
	 *            "yyyy-MM-dd HH:mm:ss:SSS"或"yyyy-MM-dd HH:mm:ss"。
	 */
	public static long dateTimeParse(String datetime, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			Date date = sdf.parse(datetime);
			return date.getTime();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return 0;
	}

	/**
	 * @return 返回测试时间是否在beginTime和endTime之间，如果在返回true，不在返回false。
	 * @param beginTime
	 *            开始时间。格式为："yyyy-MM-dd HH:mm:ss:SSS"或者"yyyy-MM-dd HH:mm:ss"。
	 * @param endTime
	 *            结束时间。格式同上。
	 * @param testTime
	 *            被测试的时间。格式同上。
	 * @param timeFormat
	 *            共同的格式为："yyyy-MM-dd HH:mm:ss:SSS"或"yyyy-MM-dd
	 *            HH:mm:ss"，请使用本类中提供的类型 。
	 */
	public static boolean isInTwoTime(String beginTime, String endTime,
			String testTime, String timeFormat) {
		long begin = DateUtils.dateTimeParse(beginTime, timeFormat);
		long end = DateUtils.dateTimeParse(endTime, timeFormat);
		long test = DateUtils.dateTimeParse(testTime, timeFormat);
		if (test > begin && test < end) {
			return true;
		}
		return false;
	}

	/**
	 * @param dateFormat
	 *            请使用本类提供的类型。格式："yyyy-MM-dd"或"yyyy/MM/dd"或"yyyy.MM.dd" 。
	 * @return 返回指定格式的当前日期
	 */
	public static String currentDate(String dateFormat) {
		Date d = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(d);
	}

	/**
	 * @param timeFormat
	 *            请使用本类提供的类型。格式："HH:mm:ss"或"HH:mm:ss:SSS" 。
	 * @return 返回指定格式的当前时间
	 */
	public static String currentTime(String timeFormat) {
		Date d = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);
		return sdf.format(d);
	}

	/**
	 * @param format
	 *            请使用本类提供的类型。格式："yyyy-MM-dd HH:mm:ss:SSS"或"yyyy-MM-dd HH:mm:ss"。
	 * @return 返回指定格式的当前日期时间
	 */
	public static String currentDateTime(String format) {
		Date d = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(d);
	}

	/** 获得当前时间,格式为："yyyy-MM-dd HH:mm:ss.SSS" */
	public static String currentDateTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Calendar
				.getInstance().getTime());
	}

	/**
	 * @return 返回当前系统时间的格林威治毫秒时。
	 * @param format
	 *            指定返回的值精确到秒还是毫秒。请使用本类提供的类型。格式："yyyy-MM-dd HH:mm:ss:SSS"精确到毫秒 或
	 *            "yyyy-MM-dd HH:mm:ss"精确到秒。
	 */
	public static long currentDateTimeGreenwich(String format) {
		Calendar rightNow = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String sysDatetime = sdf.format(rightNow.getTime());
		return DateUtils.dateTimeParse(sysDatetime, format);
	}

	/**
	 * 返回以毫秒为单位的当前时间。注意，当返回值的时间单位是毫秒时，值的粒度取决于底层操作系统，并且粒度可能更大。
	 * 例如，许多操作系统以几十毫秒为单位测量时间。 请参阅 Date 类的描述，了解可能发生在“计算机时间”和协调世界时
	 * （UTC）之间的细微差异的讨论。 返回： 当前时间与协调世界时 1970 年 1 月 1 日午夜之间的时间差（以毫秒为单 位测量）。
	 * 
	 * @see Date
	 */
	public static long currentTimeMillis() {
		return System.currentTimeMillis();
	}
	/**
	 * 获取当前时间，以timestamp的形式返回
	 * @return
	 */
	public static Timestamp currentTime(){
		return new Timestamp(new Date().getTime());
	}
	
	/**
	 * 获取当前时间，以timestamp的形式返回
	 * @return
	 */
	public static java.sql.Date currentDate(){
		return new java.sql.Date(new Date().getTime());
	}


	/**
	 * 返回最准确的可用系统计时器的当前值，以毫微秒为单位。此方法只能用于测量已过的时间，与系统或钟表时间的其他任何时间概念无关。
	 * 返回值表示从某一固定但任意的时间算起的毫微秒数（或许从以后算起，所以该值可能为负）。此方法提供毫微秒的精度，但不是必要的毫
	 * 微秒的准确度。它对于值的更改频率没有作出保证。在取值范围大于约 292 年（263 毫微秒）的连续调用的不同点在于：由于数字溢出，
	 * 将无法准确计算已过的时间。
	 */
	// 例如，测试某些代码执行的时间长度：
	// long startTime = System.nanoTime();
	// //... the code being measured ...
	// long estimatedTime = System.nanoTime() - startTime;
	//
	public static long nanoTime() {
		return System.nanoTime();
	}

	/**
	 * @return 把毫秒时间转换成小时分钟秒格式 HH:mm:ss
	 */
	public static String toHHmmss(final long millisecond) {
		long h = millisecond / _HH;
		// int m = (int) ((millisecond - h * _HH) / _MM);
		// int s = (int) ((millisecond - h * _HH - m * _MM) / _SS);
		SimpleDateFormat sdf = new SimpleDateFormat(":mm:ss");
		return h + sdf.format(millisecond);
	}

	/**
	 * @return 把毫秒时间转换成小时分钟秒毫秒格式 HH:mm:ss:SSS
	 */
	public static String toHHmmssSSS(final long millisecond) {
		long h = millisecond / _HH;
		// int m = (int) ((millisecond - h * _HH) / _MM);
		// int s = (int) ((millisecond - h * _HH - m * _MM) / _SS);
		// int ms = (int) (millisecond - h * _HH - m * _MM - s * _SS);
		SimpleDateFormat sdf = new SimpleDateFormat(":mm:ss:SSS");
		return h + sdf.format(millisecond);
	}
	
	/**
	 * 
	 * @param month 任意整数
	 * @return 返回  month 个月时间
	 */
	public static Date getTimeByMonth(int month){
		Calendar time=Calendar.getInstance();
		time.set(Calendar.MONTH, month);
		return time.getTime();
	}

	/**
	 * 指定日期上加/减相应的数值后得到新的日期值
	 * @param d 要计算的日期
	 * @param field 要操作的日期字段.<pre>{@link Calendar#YEAR},
	 * {@link Calendar#MONTH},
	 * {@link Calendar#DAY_OF_MONTH},
	 * {@link Calendar#HOUR},
	 * {@link Calendar#MINUTE},
	 * {@link Calendar#SECOND},
	 * {@link Calendar#MILLISECOND}</pre>
	 * @param amount 加/减数量.
	 * @return
	 */
	public static Date add(Date d, int field, int amount){
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(field, amount);
		return cal.getTime();
	}
	/**
	 * 指定日期加/减相应的秒数量后得到新的日期值.
	 * @param d
	 * @param amount
	 * @return
	 */
	public static Date addSecond(Date d,  int amount){
		return add(d, Calendar.SECOND, amount);
	}

	/**
	 * 将 formatTime 按照 dateformat 格式化输出
	 * @param formatTime
	 * @param dateformat
	 * @return
	 * @throws ParseException 
	 */
	public static Date formatStr2Date(String formatTime, String dateformat) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
		return sdf.parse(formatTime);
	}
	
	/**
	 * 获取当前年份最开始的时间 eg：2013-->2013-01-01 00:00:00
	 * @return
	 * @throws ParseException 
	 */
	public static Timestamp getTimeThisYear() throws ParseException {
		Calendar time=Calendar.getInstance();
		int year = time.get(Calendar.YEAR);
		String thisYear = year + "-01-01 00:00:00";
		return new Timestamp(formatStr2Date(thisYear,DateTimeFormat1).getTime());
	}
	
	/**
	 * 指定时间和当前时间比较<br>
	 * 如果比当前时间早，返回 false
	 * 如果比当前时间晚，返回 true
	 * 
	 * @param d 指定时间
	 * @return
	 */
	public static boolean compareWithCurrDate(Date d){
		long cur= currentTimeMillis();
		long tem = d.getTime();
		if(cur<tem){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 将 formatTime 按照 dateformat 格式化输出
	 * @param formatTime
	 * @param dateformat
	 * @return
	 * @throws ParseException 
	 */
	public static String formatTimer2Str(Timestamp formatTime, String dateformat) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
		return sdf.format(formatTime);
	}

	/**
	 * 格式化时间
	 */
	public static Date getRightDate(String date) {
		judgeNull(date);
		SimpleDateFormat sdf = new SimpleDateFormat(DateTimeFormat1);
		Date parse = null;
		try {
			parse = sdf.parse(date);
		} catch (ParseException e) {
			throw new BizException("格式化时间失败");
		}
		return parse;
	}

	/**
	 * 格式化时间
	 */
	public static Date getDate(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DateTimeFormat1);
		Date parse = null;
		try {
			parse = sdf.parse(date);
		} catch (ParseException e) {
			throw new BizException("格式化时间失败");
		}
		return parse;
	}







	/**
	 * @return 格式化后的时间字符串。
	 */
	public static String getDateString(Date date) {
		ObjectUtils.isNull(date, "时间date为空");
		String flag=null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DateTimeFormat1);
			flag = sdf.format(date);
		} catch (Exception ex) {
			throw new BizException("获取时间字符串失败");
		}
		return flag;
	}



	/**
	 * 判断回传的时间是否是null 或空串
	 */
	public static void judgeNull(String dateString) {
		if (null == dateString || "".equals(dateString)) {
			throw new BizException("时间字符串为空");
		}
	}
}
