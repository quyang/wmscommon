package com.xianzaishi.wms.common.utils.worker;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class WorkerForMutil extends Worker {
	private static Log log = LogFactory.getLog(WorkerForMutil.class);
	protected volatile AtomicInteger counter = null;
	protected int total = 0;

	public void run() {
		try {
			result = execute();
			success = true;
		} catch (Exception e) {
			success = false;
			log.error("worker for mutil error : ", e);
		}
		if (counter.incrementAndGet() == total) {
			synchronized (counter) {
				counter.notify();
			}
		}
	}

	public AtomicInteger getCounter() {
		return counter;
	}

	public void setCounter(AtomicInteger counter) {
		this.counter = counter;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
}
