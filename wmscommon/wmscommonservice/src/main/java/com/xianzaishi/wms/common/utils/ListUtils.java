package com.xianzaishi.wms.common.utils;

import com.xianzaishi.wms.common.exception.BizException;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;

/**
 * Created by quyang on 2017/5/6.
 */
public class ListUtils {


  /**
   * 判断集合是否为空
   * @param list
   */
  public static void isEmpty(List list,String showMsg) {
    if (CollectionUtils.isEmpty(list)) {
      throw new BizException(showMsg);
    }
  }

  /**
   * 获取集合大小
   * @param list
   */
  public static Integer getListSize(List list) {
    isEmpty(list,"集合为空");
    return list.size();
  }

  /**
   * 判断集合是否含有元素element(long)
   * @param list
   */
  public static Boolean hasElementLongOfList(List<Long> list,Long element) {
    isEmpty(list,"集合为空");
    ObjectUtils.isNull(element, "集合中的元素" + element + "为空");
    Boolean a = false;
    for (Long one:list) {
      if (null == one) {
        continue;
      }
      if (list.contains(element)) {
        a = true;
      }
    }
    return a;
  }

  /**
   * 判断集合是否含有元素element(int)
   * @param list
   */
  public static Boolean hasElementIntOfList(List<Integer> list,Integer element) {
    isEmpty(list,"集合为空");
    ObjectUtils.isNull(element,"集合中的元素" + element + "为空");
    Boolean a = false;
    for (Integer one:list) {
      if (null == one) {
        continue;
      }
      if (list.contains(element)) {
        a = true;
      }
    }
    return a;
  }

  /**
   * 判断集合是否含有元素element(int)
   * @param list
   */
  public static void removeElementIntOfList(List<Integer> list,Integer element) {
    isEmpty(list,"集合为空");
    ObjectUtils.isNull(element,"集合中的元素" + element + "为空");
    if (hasElementIntOfList(list, element)) {
      list.remove(element);
    }
  }

  /**
   * 判断集合是否含有元素element(int)
   * @param list
   */
  public static void removeElementLongOfList(List<Long> list,Long element) {
    isEmpty(list,"集合为空");
    ObjectUtils.isNull(element, "集合中的元素" + element + "为空");
    if (hasElementLongOfList(list, element)) {
      list.remove(element);
    }
  }
}
