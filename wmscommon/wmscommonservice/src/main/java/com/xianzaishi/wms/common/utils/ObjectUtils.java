package com.xianzaishi.wms.common.utils;

import com.xianzaishi.wms.common.exception.BizException;

/**
 * Created by quyang on 2017/5/6.
 */
public class ObjectUtils {


  public static void isNull(Object obj, String showMsg) {
    if (null == obj) {
      throw new BizException(showMsg);
    }
  }

  public static void isAllNull(String showMsg, Object... obj) {
    isNull(obj, "可变参数为空");
    int length = obj.length;
    int a = 0;
    for (Object object : obj) {
      if (null == object) {
        a = a + 1;
      }
    }
    if (a == length) {
      throw new BizException(showMsg);
    }
  }
}
