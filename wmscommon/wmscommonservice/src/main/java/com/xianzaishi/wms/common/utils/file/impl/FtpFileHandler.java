package com.xianzaishi.wms.common.utils.file.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPReply;

import com.xianzaishi.wms.common.utils.StringUtils;
import com.xianzaishi.wms.common.utils.file.IRemoteFileHandler;

/**
 * 远程FTP文件处理器.
 * <p>
 * 将文件上传到指定FTP服务器. 分布式环境使用.
 * </p>
 * 
 * @author lirui
 * 
 */
public class FtpFileHandler implements IRemoteFileHandler {
	private static final Log log = LogFactory.getLog(FtpFileHandler.class);
	private FTPClient ftp = null;
	private String server = null;
	private Integer port = null;
	private String username = null;
	private String password = null;

	public String saveFile(InputStream in, String baseDir, String subdir,
			String filename) {
		String pathdir = StringUtils.join("/", baseDir, subdir);
		try {
			prepareDir(pathdir);
			transFile(filename, in);
		} catch (IOException e) {
			log.error("ftp error:", e);
		}
		return filename;
	}

	private void transFile(String filename, InputStream in) throws IOException {
		log.debug("##FTP## transfering file..");
		long t = System.currentTimeMillis();
		boolean success = ftp.storeFile(filename, in);
		long cost = System.currentTimeMillis() - t;
		log.debug("##FTP## finish transfering file.Cost(ms):" + cost);
		if (!success) {
			log.error("##FTP## Transfering file faild. " + ftp.getReplyString());
			throw new IOException("Transfering file faild. "
					+ ftp.getReplyString());
		}
	}

	private void prepareDir(String path) throws IOException {
		// TODO Auto-generated method stub
		log.debug("##FTP## prepare directory " + path);
		String[] paths = path.split("/");

		// 逐级检查目录是否存在,如不存在则建立,存在则进入.
		for (String p : paths) {
			if (!StringUtils.isEmpty(p)) {
				int rep = ftp.cwd(p);// try to go into
				if (!FTPReply.isPositiveCompletion(rep)) {
					// 550 x: direcotry not exists. try to create.
					if (rep == 550) {
						log.debug("##FTP## Dir " + p
								+ " does not exist. Trying creating.");
						if (!ftp.makeDirectory(p)) {
							// make dir failed.
							log.error("##FTP## failed to make dir;"
									+ ftp.getReplyString());
							throw new IOException(
									"Cannot make dir through FTP."
											+ ftp.getReplyString());
						} else {
							// make dir successfully.
							ftp.cwd(p);
						}
					}
					// otherwise: throw exception
					else {
						log.error("##FTP## change to dir faild;"
								+ ftp.getReplyString());
						throw new IOException(
								"Cannot change working directory " + p
										+ ftp.getReplyString());
					}
				}
			}
		}
	}

	/**
	 * 建立连接
	 */
	public boolean connect() {
		ftp = new FTPClient();
		FTPClientConfig config = new FTPClientConfig();
		ftp.configure(config);
		boolean success = true;
		try {
			int reply;
			ftp.connect(server);
			log.debug("##FTP## Connected to " + server + ".");
			log.debug("##FTP## " + ftp.getReplyString());

			// After connection attempt, you should check the reply code to
			// verify
			// success.
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				success = false;
				log.error("##FTP## FTP server refused connection.");
			}
			success = success && ftp.login(username, password);
			if (!success) {
				log.error("##FTP## login FTP server failed.");
				return success;
			}
			success = success && ftp.setFileType(FTP.BINARY_FILE_TYPE);
			if (!success) {
				log.error("##FTP## set file type as BINARY failed.");
				return success;
			}
			success = true;
		} catch (IOException e) {
			success = false;
			log.error("ftp error:", e);
		}
		return success;
	}

	public void disconnect() {
		try {
			if (ftp != null)
				ftp.disconnect();
		} catch (Exception e) {
			log.error("ftp error:", e);
		}
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFile(String inFileName, String baseDir, String subdir,
			String filename) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	public InputStream getInputStream(String fileName) throws IOException {
		return ftp.retrieveFileStream(fileName);
	}

	public OutputStream getOutputStream(String fileName) throws IOException {
		prepareDir(fileName.substring(fileName.lastIndexOf("/")));
		return ftp.storeFileStream(fileName);
	}

	public static void main(String[] args) {
	}
}
