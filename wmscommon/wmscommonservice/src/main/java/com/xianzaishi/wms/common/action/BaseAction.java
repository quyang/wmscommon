package com.xianzaishi.wms.common.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.CookiesAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.util.ServletContextAware;

import com.xianzaishi.wms.common.lang.KeyValue;
import com.xianzaishi.wms.common.utils.StringUtils;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @Title: BaseAction.java
 * @Package com.loveyfruit.base.action
 * @author WangShanShan
 * @version V1.0
 * 
 */

@SuppressWarnings("serial")
public abstract class BaseAction extends ActionSupport  implements ServletContextAware, ServletRequestAware, ServletResponseAware, CookiesAware{
	
	/**
	 * XHR请求响应代码
	 */
	public class XhrStatusCode{
		/** 跳转到指定的URL */
		public static final int NO_AUTH = 401;
		public static final int REDIRECT = 302;
	}
	
	protected ServletContext servletContext;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected Map<String, String> cookies;
	protected String MSG = "";
	private String xhr;
	
	/**
	 * 返回json信息
	 */
	protected Map<String,Object> dataMap = new HashMap<String,Object>();
	
	/**
	 * 针对XHR请求给出统一的下一步操作. <br>
	 * 调用此方法会在{@link dataMap} 中设置一个属性名为<code>next</code>的
	 * {@link KeyValue} 对象,key为响应动作编码({@link XhrStatusCode}), value为动作参数. 
	 * @param statusCode
	 * @param param
	 */
	protected void setNextActForXHR(int statusCode, String param){
		KeyValue<Integer, String> next = new KeyValue<Integer, String>(statusCode, param);
		dataMap.put("next", next);
	}
	
	protected boolean isXhr(){
		return StringUtils.isNotEmpty(xhr);
	}
	
	public void setServletContext(ServletContext context) {
		servletContext = context;
	}
	

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getMSG() {
		return MSG;
	}

	public void setMSG(String mSG) {
		MSG = mSG;
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}


	public void setCookiesMap(Map<String, String> cookies) {
		this.cookies = cookies;
	}

	public void setXhr(String xhr) {
		this.xhr = xhr;
	}

}
