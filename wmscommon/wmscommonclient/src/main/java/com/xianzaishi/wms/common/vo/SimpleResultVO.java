package com.xianzaishi.wms.common.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.exception.BizException;

public class SimpleResultVO<T> implements Serializable {
	public transient static Short RETURN_SUCCESS = 0;
	public transient static Short RETURN_ERROR = 1;
	public transient static String UNKNOWN_ERROR = "unknown error";

	private Short return_code = SimpleResultVO.RETURN_ERROR;
	private String message = null;
	private T target = null;

	/**
	 * 合并return_code和message
	 * 
	 * @param result
	 */
	public void mergeResult(SimpleResultVO<T> result) {
		this.return_code = (short) (this.return_code | result.getReturn_code());
		if (this.message != null && result.getMessage() != null) {
			this.message += result.getMessage();
		} else if (result.getMessage() != null) {
			this.message = result.getMessage();
		}
	}

	public static SimpleResultVO buildBizErrorResult(BizException e) {
		SimpleResultVO simpleResultVO = new SimpleResultVO<>();
		simpleResultVO.setMessage(e.getMessage());
		return simpleResultVO;
	}

	public static SimpleResultVO buildErrorResult() {
		SimpleResultVO simpleResultVO = new SimpleResultVO<>();
		simpleResultVO.setMessage(UNKNOWN_ERROR);
		return simpleResultVO;
	}

	public static SimpleResultVO buildSuccessResult(Object object) {
		SimpleResultVO simpleResultVO = new SimpleResultVO<>();
		simpleResultVO.setReturn_code(RETURN_SUCCESS);
		simpleResultVO.setTarget(object);
		return simpleResultVO;
	}

	/**
	 * 设置成功状态.
	 * 
	 * @param target
	 */
	public void markSuccess(T target) {
		this.target = target;
		this.return_code = SimpleResultVO.RETURN_SUCCESS;
	}

	/**
	 * 设置失败状态
	 * 
	 * @param message
	 */
	public void markFaild(String message) {
		this.message = message;
		this.return_code = SimpleResultVO.RETURN_ERROR;
	}

	/**
	 * 判断返回码是否为成功状态.
	 * 
	 * @return
	 */
	public boolean isSuccess() {
		return SimpleResultVO.RETURN_SUCCESS.equals(return_code);
	}

	public Short getReturn_code() {
		return return_code;
	}

	public void setReturn_code(Short return_code) {
		this.return_code = return_code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getTarget() {
		return target;
	}

	public void setTarget(T target) {
		this.target = target;
	}
}
