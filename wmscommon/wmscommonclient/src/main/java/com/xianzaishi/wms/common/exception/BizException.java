package com.xianzaishi.wms.common.exception;

/**
 * 
 * 业务异常封装
 * 
 * @author yuchao1
 * 
 */
public class BizException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BizException() {
		super();
	}

	public BizException(String message, Throwable cause) {
		super(message, cause);
	}

	public BizException(String message) {
		super(message);
	}

	public BizException(Throwable cause) {
		super(cause);
	}

}
