package com.xianzaishi.wms.common.vo;

import java.io.Serializable;

public class SimpleRequestVO<T> implements Serializable {
	private String token = null;
	private T data = null;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
