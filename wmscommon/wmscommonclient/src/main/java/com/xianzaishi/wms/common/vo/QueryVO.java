package com.xianzaishi.wms.common.vo;

import java.io.Serializable;
import java.util.Date;

public class QueryVO implements Serializable {
	private Integer page = 1;
	private Integer size = PAGESIZE;
	private Integer totalPage = null;
	private Integer totalRecord = null;

	protected Long id = null;

	private Short dr = null;

	private Date ts = null;

	public Integer getTotalPage() {
		if (totalPage == null && totalRecord != null) {
			totalPage = totalRecord / size + 1;
		}
		return totalPage;
	}

	public Boolean hasNextPage() {
		Boolean result = false;
		Integer flag = getTotalPage();
		if (page != null && flag != null && page < flag && page >= 0) {
			result = true;
		}
		return result;
	}

	public Boolean hasPreviewPage() {
		Boolean result = false;
		Integer flag = getTotalPage();
		if (page != null && flag != null && page < flag && page > 1) {
			result = true;
		}
		return result;
	}

	public Integer getNextPageNo() {
		if (hasNextPage())
			return page + 1;
		return null;
	}

	public Integer getPreviewPageNo() {
		if (hasPreviewPage())
			return page - 1;
		return null;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Integer getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(Integer totalRecord) {
		this.totalRecord = totalRecord;
	}

	public transient final static Integer PAGESIZE = 10;

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public int getStartRow() {
		if (page != null && size != null && page > 0) {
			return (page - 1) * size;
		} else {
			return 0;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public Short getDr() {
		return dr;
	}

	public void setDr(Short dr) {
		this.dr = dr;
	}
}
