package com.xianzaishi.wms.common.vo;

import java.io.Serializable;
import java.util.List;

public class QueryResultVO<T> implements Serializable {
	private Integer page = 1;

	private Integer size = 10;

	private Integer totalCount = 0;

	private Integer pageCount = 0;

	private List<T> items = null;

	public QueryResultVO() {
	}

	public QueryResultVO(Integer page, Integer size, Integer totalCount) {
		super();
		this.page = (page == null ? 1 : page);
		this.size = (size == null ? 10 : size);
		this.totalCount = (totalCount == null ? 0 : totalCount);
		this.pageCount = 0;
	}

	public Integer getPreviousPage() {
		return page == null ? 1 : (page <= 1 ? 1 : page - 1);
	}

	public Integer getNextPage() {
		return page == null ? 1 : (getLastPage() ? pageCount : page + 1);
	}

	public boolean getFirstPage() {
		return page == null || page == 1;
	}

	public boolean getLastPage() {
		return page == null || pageCount == null || page.equals(pageCount);
	}

	public Integer getPageCount() {
		if (pageCount == null || pageCount == 0) {
			if (totalCount != null && size != null) {
				pageCount = totalCount / size;
				if (totalCount != 0)
					pageCount += 1;
			}
		}
		if (pageCount == null) {
			pageCount = 0;
		}
		return pageCount;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

}
