package com.xianzaishi.wms.base.test;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class BaseTest extends AbstractJUnit4SpringContextTests {
	static {
		PropertyConfigurator.configure(Test.class.getClassLoader().getResource(
				"log4j/log4j.properties"));
	}
}
